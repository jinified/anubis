## Students

## Get all students

```bash
http://localhost/v1/students
```

## Get all female students

```bash
http://localhost/v1/students?gender=F
```

## Get all chinese male students

```bash
http://localhost/v1/students?gender=M&race=chinese
```

## Get all female students ordered by id

```bash
http://localhost/v1/students?gender=F&orderBy=id
```

## Export to CSV

```bash
http://localhost/v1/students?id:lt=5&is_export=true
```

## Add students (JSON)

```bash
curl -i -d '{"students": [{"ic":"050811071188","id":"11","name":"NUR BAINI BINTI AHMAD SYAH","gender":"M","race":"malay","religion":"islam","class":"1CERDAS","merit":50}]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost/v1/students
```

## Update students (JSON)

```bash
curl -i -d '{"students": [{"ic":"050811071188","id":"13","name":"NUR BAINI BINTI AHMAD SYAH","gender":"F","race":"chinese","religion":"islam","class":"1CERDAS","merit":50}]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost/v1/students
```

## Add/Update students (CSV)

```bash
cd ~/projects/anubis/data;
curl -i -F 'file=@insert-students.csv' http://localhost/v1/students/upload-csv
```

## Delete students

```bash
curl -i -d '{"students": [2, 4]}' -H "Content-Type: application/json" -H "Accept: application/json" -X DELETE http://localhost/v1/students
```

## Reset merits of students

```bash
curl -i -d '{"students": [2, 4]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost/v1/students/reset
```

## Archive students

```bash
curl -i -d '{"students": [2, 4]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost/v1/students/archive
```

---

## Cases

## Export to CSV

```bash
http://localhost/v1/cases?id:lt=5&is_export=true
```

## Add cases (JSON)

```bash
 curl -i -d '{"cases": [{"student_id": "6", "teacher_id": "6", "merit": -30, "code": "A01", "description": "eat in class", "date": "2018-05-10", "time": "13:11"}]}' -H "Content-Type: application/json" -H "Accept: application/json" -X POST http://localhost/v1/cases
```

## Add/Update cases (CSV)

```bash
cd ~/projects/anubis/data;
curl -i -F 'file=@insert-cases.csv' http://localhost/v1/cases/upload-csv 
```

## Delete cases

```bash
curl -i -d '{"cases": [1, 2]}' -H "Content-Type: application/json" -H "Accept: application/json" -X DELETE http://localhost/v1/cases
```

---

## Authentication

## Register user

```bash
curl -i -d '{"username": "testuser", "email": "testuser@example.com", "password": "kamekameha"}' -X POST http://localhost/v1/login/register
```
## Wrong Login

```bash
curl -i -X POST http://localhost/v1/login/username&wrongpassword
```

## Correct Login

```bash
curl -i -X POST http://localhost/v1/login/username&correctpassword
```
