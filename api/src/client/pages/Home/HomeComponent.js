import React from 'react';
import PropTypes from 'prop-types';
import Dashboard from '../../components/Dashboard/DashboardContainer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Container, Title } from 'bloomer';

const HomeComponent = (props) => {
    return (
        <Container style={{
            marginTop: '20px',
            marginBottom: '20px',
            minHeight: '800px',
        }}>
            <Title isSize={2} style={{
                textAlign: 'center'
            }}>{'Dashboard'}</Title>
            <Dashboard />
        </Container>
    );
}


export default HomeComponent;
