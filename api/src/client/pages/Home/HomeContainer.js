import React from 'react';
import Home from './HomeComponent';
import Header from '../../components/Header/HeaderContainer';
import { Redirect } from 'react-router-dom';

export default class HomeContainer extends React.Component {
    render() {
        let user = JSON.parse(localStorage.getItem('user'))
        if (!user) {
            return <Redirect to="/login" />;
        } else {
            return (
                <div>
                    <Header />
                    <Home />
                </div>
            );
        }
    }
}



