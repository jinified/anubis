import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';
import ReactTooltip from 'react-tooltip';

const makeTooltip = (options) => {
    return (
        <div 
            data-tip={options.tip}
            data-type='info'
        >
            <h1>{options.header}</h1>
            <ReactTooltip multiline={true} />
        </div>
    )
}

const InsertRow = (props) => {
    return (
    <ReactTable
        data={props.userInput}
        style={{ marginBottom: '20px' }}
        showPagination={false}
        defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value
        }
        columns={[
            {
                Header: x => {
                    return makeTooltip({
                        header: 'ID',
                        tip: 'Student id'
                    })
                },
                accessor: "id",
                Cell: props.renderCell['id'](),
            },
            {
                Header: x => {
                    return makeTooltip({
                        header: 'IC',
                        tip: 'Length 12. Numbers only',
                    })
                },
                accessor: "ic",
                Cell: props.renderCell['ic'](),
            },
            {
                Header: "Name",
                accessor: "name",
                Cell: props.renderCell['name'](),
            },
            {
                Header: "CLASS",
                accessor: "class",
                Cell: props.renderCell['class']([
                    ['1CERDAS', '1 Cerdas'],
                    ['1PINTAR', '1 Pintar'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
            },
            {
                Header: "GENDER",
                accessor: "gender",
                Cell: props.renderCell['gender']([
                    ['F', 'Female'],
                    ['M', 'Male'],
                ]),
            },
            {
                Header: "RACE",
                accessor: "race",
                Cell: props.renderCell['race']([
                    ['chinese', 'Chinese'],
                    ['malay', 'Malay'],
                    ['indian', 'Indian'],
                ]),
            },
            {
                Header: "RELIGION",
                accessor: "religion",
                Cell: props.renderCell['religion']([
                    ['buddha', 'Buddhism'],
                    ['islam', 'Islam'],
                    ['hindu', 'Hinduism'],
                ]),
            },
            {
                Header: "MERIT",
                accessor: "merit",
                Cell: props.renderCell['merit']({
                    min: '1',
                    max: '100',
                    defaultValue: '100'
                }),
            },
        ]}
        defaultPageSize={1}
        className="-striped -highlight"
    >
        {(state, makeTable, instance) => {
            return (
                <div>
                    {makeTable()}
                    <Button isColor='success' style={{ marginBottom: '50px' }}
                        onClick={() => props.handleOnSubmit(state)}
                    >Submit</Button>
                </div>
            );
        }}
    </ReactTable>
    );
}

export default InsertRow;
