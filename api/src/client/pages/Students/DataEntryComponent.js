import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { CSVLink, CSVDownload } from 'react-csv';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const axios = require('axios')
const apiHost = process.env.apiHost

const DataEntry = (props) => {
    return (
        <div style={{ 
            marginTop: '30px'
        }}>
            <ReactTable
                data={props.transactions}
                filterable
                defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value
                }
                columns={[...props.columns, 
                {
                    Header: "Actions",
                    id: 'action',
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <Button
                                isColor='primary'
                                isOutlined
                                onClick={() => {
                                    console.log(original.id)
                                    props.removeRow(original.id)
                                }}
                            >
                                <FontAwesomeIcon icon='times' size='sm' />
                            </Button>
                        );
                    },
                    filterable: false,
                }]}
                defaultSorted={[
                    {
                        id: "id"
                    }
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
             >
                {(state, makeTable, instance) => {
                    console.log('Updated')
                    return (
                        <div>
                            {makeTable()}
                            <Button isColor='info' isOutlined
                                onClick={() => {
                                    let data = state.sortedData.map((row) => { return row._original; })
                                    axios.post(`http://${apiHost}/v1/students`, {'students': data})
                                    .then(function (response) {
                                        console.log(response);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }}
                                style={{ marginTop: '20px', marginRight: '20px'}}>Add/Update</Button>
                            <Button isColor='info' isOutlined
                                onClick={() => {
                                    let content = state.sortedData.map((row) => { return row._original.id; })
                                    // Need to specify request body
                                    axios.post(`http://${apiHost}v1/students/archive`,  {'students': content})
                                    .then(function (response) {
                                        console.log(response);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }}
                                style={{ marginTop: '20px', marginRight: '20px'}}>Archive</Button>
                            <Button isColor='info' isOutlined
                                onClick={() => {
                                    let content = state.sortedData.map((row) => { return row._original.id; })
                                    // Need to specify request body
                                    axios.post(`http://${apiHost}/v1/students/reset`,  {'students': content})
                                    .then(function (response) {
                                        console.log(response);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }}
                                style={{ marginTop: '20px', marginRight: '20px'}}>Reset Merit</Button>
                            <Button isColor='danger' isOutlined
                                onClick={() => {
                                    let content = state.sortedData.map((row) => { return row._original.id; })
                                    // Need to specify request body
                                    axios.delete(`http://${apiHost}/v1/students`, { data: {'students': content}})
                                    .then(function (response) {
                                        console.log(response);
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }}
                                style={{ marginTop: '20px', marginRight: '20px'}}>Delete</Button>
                        </div>
                    );
                }}
            </ReactTable>
        </div>
    );
}

export default DataEntry;
