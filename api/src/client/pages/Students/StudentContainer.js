import React from 'react';
import { Container, Title } from 'bloomer';
import { Redirect, Switch, Route, Link } from "react-router-dom";
import Header from '../../components/Header/HeaderContainer';
import ResultPanel from '../../components/ResultPanel/ResultPanelContainer';
import DataEntry from './DataEntryContainer';
import { Tabs, TabList, Tab, TabLink } from 'bloomer';
import ReactTooltip from 'react-tooltip';

const axios = require('axios');
const jwt = require('jsonwebtoken')
const apiHost = process.env.apiHost

export default class StudentContainer extends React.Component {
    constructor() {
        super();
        this.columns = [
            {
                Header: "ID",
                accessor: "id",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(original.id,
                        StudentContainer.isDigit,
                        'Numbers only');
                },
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "IC",
                id: 'ic',
                accessor: "ic",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(original.ic,
                        StudentContainer.validateIC,
                        'Numbers only<br/>Length: 12');
                },
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().startsWith(filter.value.toUpperCase())
            },
            {
                Header: "Name",
                accessor: "name",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(original.name,
                        StudentContainer.isAlphabet,
                        'Alphabets only');
                },
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "CLASS",
                accessor: "class",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(
                        original.class, StudentContainer.validateSelections,
                        'Options not supported',
                        {selections: ['1CERDAS', '1PINTAR', '1TEKUN', 'ARCHIVED']}
                    );
                },
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="1 CERDAS">1CERDAS</option>
                    </select>
            },
            {
                Header: "GENDER",
                accessor: "gender",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(
                        original.gender, StudentContainer.validateSelections,
                        'Options not supported',
                        {selections: ['M', 'F']}
                    );
                },
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
            },
            {
                Header: "RACE",
                accessor: "race",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(
                        original.race, StudentContainer.validateSelections,
                        'Options not supported',
                        {selections: ['chinese', 'malay', 'indian']}
                    );
                },
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="chinese">Chinese</option>
                        <option value="malay">Malay</option>
                        <option value="indian">Indian</option>
                    </select>
            },
            {
                Header: "RELIGION",
                accessor: "religion",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(
                        original.religion, StudentContainer.validateSelections,
                        'Options not supported',
                        {selections: ['buddha', 'hindu', 'islam']}
                    );
                },
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="buddha">Buddha</option>
                        <option value="islam">Islam</option>
                        <option value="hindu">Hindu</option>
                    </select>
            },
            {
                Header: "MERIT",
                accessor: "merit",
                Cell: ({ original }) => {
                    return StudentContainer.renderCell(
                        original.merit, StudentContainer.validateMerit,
                        'Must between -100 to 100',
                        {range: [0, 100]}
                    );
                },
                filterMethod: (filter, row) => {
                    return row[filter.id] <= filter.value;
                },
                Filter: ({ filter, onChange }) => 
                    <div>
                        <div>
                            <input type='number' min="1" max="100" id="merit_range" defaultValue="100"
                                onChange={event => {
                                    onChange(event.target.value);
                                }}
                                style={{ width: '80px' }}
                            />
                        </div>
                        <div>
                            <input type="range" min="0" max="100" step="1" 
                                value={filter ? filter.value : "100"}
                                style={{ width: "100%" }}
                                name='rangeVal'
                                id='rangeVal'
                                onChange={event => {
                                    onChange(event.target.value);
                                    document.getElementById("merit_range").value = event.target.value;
                                }} />
                        </div>
                    </div>
            },
        ]
    }

    state = {
        students: [],
    }

    componentDidMount() {
        axios.get(`http://${apiHost}/v1/students`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { students: res.data } 
                });
            })
    }


    static renderCell(input, validator, errorMsg, options={}) {
        if (validator(input, options)) {
            return (
                <div>{input}</div>
            )
        } else {
            return input
            ? (<div data-tip={errorMsg} data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
            : (<div data-tip='Required' data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
        }
    }

    static validateSelections(input, options) {
        return options.selections.indexOf(input) >= 0;
    }

    static validateMerit(input, options) {
        let num = parseInt(input)
        return StudentContainer.isDigit(input) && (num >= options.range[0] && num <= options.range[1]);
    }


    static validateIC(input) {
        return input.length === 12 && StudentContainer.isDigit(input)
    }

    static isDigit(input) {
        return /^\d+$/.test(input);
    }

    static isAlphabet(input) {
        return /[a-z]/i.test(input);
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'))
        if (!user) {
            return <Redirect to='/login' />
        } else {
            let decoded = jwt.decode(user.refreshToken)
            let currentTime = Date.now() / 1000
            if (decoded.exp < currentTime) {
                localStorage.removeItem('user')
                return <Redirect to='/login' />
            } else  {
                return (
                    <div>
                        <Header />
                        <Container style={{
                            marginTop: '20px',
                            marginBottom: '20px',
                            minHeight: '800px',
                        }}>
                            <Title isSize={2} style={{
                                textAlign: 'center'
                            }}>{'Students'}</Title>
                            <Switch>
                                <Route exact path="/students" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab isActive>
                                                        <Link to='/students'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab>
                                                        <Link to='/students/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <ResultPanel columns={this.columns} data={this.state.students} />
                                        </div>
                                    );
                                }} />
                                <Route path="/students/edit" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab>
                                                        <Link to='/students'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab isActive>
                                                        <Link to='/students/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <DataEntry columns={this.columns} />
                                        </div>
                                    );
                                }} />
                            </Switch>
                        </Container>
                    </div>
                );
            }
        }
    }
}



