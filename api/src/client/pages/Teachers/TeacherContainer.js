import React from 'react';
import { Container, Title } from 'bloomer';
import { Redirect, Switch, Route, Link } from "react-router-dom";
import ResultPanel from '../../components/ResultPanel/ResultPanelContainer';
import DataEntry from './DataEntryContainer';
import { Tabs, TabList, Tab, TabLink } from 'bloomer';
import ReactTooltip from 'react-tooltip';
import Header from '../../components/Header/HeaderContainer';

const axios = require('axios');
const jwt = require('jsonwebtoken')
const apiHost = process.env.apiHost

export default class TeacherContainer extends React.Component {
    constructor() {
        super();
        this.columns = [
            {
                Header: "ID",
                accessor: "id",
                Cell: ({ original }) => {
                    return TeacherContainer.renderCell(original.id,
                        TeacherContainer.isDigit,
                        'Numbers only');
                },
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "IC",
                id: 'ic',
                accessor: "ic",
                Cell: ({ original }) => {
                    return TeacherContainer.renderCell(original.ic,
                        TeacherContainer.validateIC,
                        'Numbers only<br/>Length: 12');
                },
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().startsWith(filter.value.toUpperCase())
            },
            {
                Header: "Name",
                accessor: "name",
                Cell: ({ original }) => {
                    return TeacherContainer.renderCell(original.name,
                        TeacherContainer.isAlphabet,
                        'Alphabets only');
                },
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "CLASS",
                accessor: "class",
                Cell: ({ original }) => {
                    return TeacherContainer.renderCell(
                        original.class, TeacherContainer.validateSelections,
                        'Options not supported',
                        {selections: ['1CERDAS', '1PINTAR', '1TEKUN', 'ARCHIVED']}
                    );
                },
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="1 CERDAS">1CERDAS</option>
                    </select>
            },
            {
                Header: "Remarks",
                accessor: "remarks",
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
        ]
    }

    state = {
        teachers: [],
    }

    componentDidMount() {
        axios.get(`http://${apiHost}/v1/teachers`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { teachers: res.data } 
                });
            })
    }


    static renderCell(input, validator, errorMsg, options={}) {
        if (validator(input, options)) {
            return (
                <div>{input}</div>
            )
        } else {
            return input
            ? (<div data-tip={errorMsg} data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
            : (<div data-tip='Required' data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
        }
    }

    static validateSelections(input, options) {
        return options.selections.indexOf(input) >= 0;
    }

    static validateIC(input) {
        return input.length === 12 && TeacherContainer.isDigit(input)
    }

    static isDigit(input) {
        return /^\d+$/.test(input);
    }

    static isAlphabet(input) {
        return /[a-z]/i.test(input);
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'))
        if (!user) {
            return <Redirect to='/login' />
        } else {
            let decoded = jwt.decode(user.refreshToken)
            let currentTime = Date.now() / 1000
            if (decoded.exp < currentTime) {
                localStorage.removeItem('user')
                return <Redirect to='/login' />
            } else  {
                return (
                    <div>
                        <Header />
                        <Container style={{
                            marginTop: '20px',
                            marginBottom: '20px',
                            minHeight: '800px',
                        }}>
                            <Title isSize={2} style={{
                                textAlign: 'center'
                            }}>{'Teachers'}</Title>
                            <Switch>
                                <Route exact path="/teachers" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab isActive>
                                                        <Link to='/teachers'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab>
                                                        <Link to='/teachers/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <ResultPanel columns={this.columns} data={this.state.teachers} />
                                        </div>
                                    );
                                }} />
                                <Route path="/teachers/edit" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab>
                                                        <Link to='/teachers'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab isActive>
                                                        <Link to='/teachers/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <DataEntry columns={this.columns} />
                                        </div>
                                    );
                                }} />
                            </Switch>
                        </Container>
                    </div>
                );
            }
        }
    }
}



