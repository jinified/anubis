import React from 'react';
import DataEntry from './DataEntryComponent';
import InsertRow from './InsertRow';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';
import ReactTable from "react-table";
import CSVReader from 'react-csv-reader';
require('lodash');

export default class DataEntryContainer extends React.Component {
    constructor() {
        super();
        this.renderNumberCell = this.renderNumberCell.bind(this);
        this.renderSelectionCell = this.renderSelectionCell.bind(this);
        this.renderTextCell = this.renderTextCell.bind(this);
        this.handleOnSubmit = this.handleOnSubmit.bind(this);
        this.setTableRef = this.setTableRef.bind(this);
        this.removeRow = this.removeRow.bind(this)
        this.clearAllRows = this.clearAllRows.bind(this)
    }

    state = {
        transactions: [],
        userInput: this.getEmptyRow(),
        dataEntry: null
    }

    getEmptyRow() {
        return [{ 
            ic: '',
            id: '',
            name: '',
            class: '',
            remarks: ''
        }];
    }

    removeRow(id) {
        this.setState((prevState, props) => {
            return { transactions: prevState.transactions.filter((row) => row.id != id) }
        })
    }

    clearAllRows() {
        this.setState((prevState, props) => {
            return { transactions: [] };
        })
    }

    setTableRef(r) {
        this.setState((prevState, props) => { return { dataEntry: r }; });
    }

    handleOnSubmit(state) {
        this.setState((prevState, props) => {
            return { transactions: [...prevState.transactions, state.sortedData[0]._original] }
            //  return { transactions: prevState.transactions.push(state.sortedData[0]) };
        });
        this.setState((prevState, props) => {
            return { userInput: this.getEmptyRow() };
        });
    }

    renderTextCell(options) {
        return (cellInfo) => {
            return (
                <input
                    type='text' 
                    maxLength='20'
                    style={{ width: '100%' }}
                    onChange={e => {
                        const userInput = [...this.state.userInput];
                        userInput[cellInfo.index][cellInfo.column.id] = e.target.value
                        this.setState((prevState, props) => {
                            return { userInput };
                        });
                    }}
                    value={cellInfo.value}
                />
            );
        }
    }

    renderSelectionCell(options) {
        return (cellInfo) => {
            return (
                <select
                    onChange={e => {
                        const userInput = [...this.state.userInput];
                        userInput[cellInfo.index][cellInfo.column.id] = e.target.value
                        this.setState((prevState, props) => {
                            return { userInput };
                        });
                    }}
                    style={{ width: '100%' }}
                >
                    <option value='all'>Choose</option>
                    {options.map((item) => <option value={item[0]}>{item[0]}</option>)}
                </select>
            );
        }
    }

    renderNumberCell(options) {
        return (cellInfo) => {
            return (
                <input
                    type='number' 
                    min={options.min}
                    max={options.max}
                    onChange={e => {
                        const userInput = [...this.state.userInput];
                        userInput[cellInfo.index][cellInfo.column.id] = e.target.value
                        this.setState((prevState, props) => {
                            return { userInput };
                        });
                    }}
                    style={{ width: '100%' }}
                />
            );
        }
    }

    mapUploadedToData(row) {
        
    }
    render() {
        return (
            <div>
                <Title isSize={5} style={{ marginBottom: '30px' }}>Import Record(s) from CSV</Title>
                <CSVReader
                    cssClass="react-csv-input"
                    onFileLoaded={(data) => {
                        let headers = data[0]
                        let inputs = data.slice(1, -1)
                        const imported = inputs.map((row) => {
                            return _.zipObject(headers, row)
                        })
                        this.setState((prevState, props) => {
                            return { transactions: [...prevState.transactions, ...imported] };
                        });
                    }}
                />
                <Title isSize={5} style={{ marginTop: '30px' }}>Add/Update Record(s)</Title>
                <InsertRow 
                    userInput={this.state.userInput}
                    handleOnSubmit={this.handleOnSubmit}
                    renderCell={{
                        ic: this.renderTextCell,
                        id: this.renderTextCell,
                        name: this.renderTextCell,
                        class: this.renderSelectionCell,
                        remarks: this.renderTextCell
                    }} 
                />
                <Button isColor='primary' style={{ marginBottom: '20px' }}
                    onClick={() => {
                        this.clearAllRows();
                    }}
                >Clear selections</Button>
                <Title isSize={5}>Record(s) that will be affected</Title>
                <DataEntry
                    columns={this.props.columns}
                    transactions={this.state.transactions}
                    removeRow={this.removeRow}
                />
            </div>
        );
    }
}

