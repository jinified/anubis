import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';


const InsertRow = (props) => {
    return (
    <ReactTable
        data={props.userInput}
        style={{ marginBottom: '20px' }}
        showPagination={false}
        defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value
        }
        columns={[
            {
                Header: "ID",
                accessor: "id",
                Cell: props.renderCell['id'](),
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "STUDENT ID",
                accessor: "student_id",
                Cell: props.renderCell['student_id'](),
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "TEACHER ID",
                accessor: "teacher_id",
                Cell: props.renderCell['teacher_id'](),
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "CODE",
                accessor: "code",
                Cell: props.renderCell['code']([
                    ['A01', 'A01'],
                    ['A02', 'A02'],
                    ['B02', 'B02'],
                    ['D03', 'D03'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
            },
            {
                Header: "DESC",
                accessor: "description",
                Cell: props.renderCell['description'](),
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "DATE",
                accessor: "date",
                Cell: props.renderCell['date'](),
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "TIME",
                accessor: "time",
                Cell: props.renderCell['time'](),
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "MERIT",
                accessor: "merit",
                Cell: props.renderCell['merit']({
                    min: '1',
                    max: '100',
                    defaultValue: '100'
                }),
                filterMethod: (filter, row) => {
                    return row[filter.id] <= filter.value;
                },
                Filter: ({ filter, onChange }) => 
                    <div>
                        <div>
                            <input type='number' min="1" max="100" id="merit_range" defaultValue="100"
                                onChange={event => {
                                    onChange(event.target.value);
                                }}
                                style={{ width: '80px' }}
                            />
                        </div>
                        <div>
                            <input type="range" min="0" max="100" step="1" 
                                value={filter ? filter.value : "100"}
                                style={{ width: "100%" }}
                                name='rangeVal'
                                id='rangeVal'
                                onChange={event => {
                                    onChange(event.target.value);
                                    document.getElementById("merit_range").value = event.target.value;
                                }} />
                        </div>
                    </div>
            },
        ]}
        defaultPageSize={1}
        className="-striped -highlight"
    >
        {(state, makeTable, instance) => {
            return (
                <div>
                    {makeTable()}
                    <Button isColor='success' style={{ marginBottom: '50px' }}
                        onClick={() => props.handleOnSubmit(state)}
                    >Submit</Button>
                </div>
            );
        }}
    </ReactTable>
    );
}

export default InsertRow;
