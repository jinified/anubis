import React from 'react';
import { Button, Container, Title } from 'bloomer';
import { Redirect, Switch, Route, Link } from "react-router-dom";
import ResultPanel from '../../components/ResultPanel/ResultPanelContainer';
import DataEntry from './DataEntryContainer';
import { Tabs, TabList, Tab, TabLink } from 'bloomer';
import moment from 'moment';
import ReactTooltip from 'react-tooltip';
import Header from '../../components/Header/HeaderContainer';

const jwt = require('jsonwebtoken')
const axios = require('axios');
const apiHost = process.env.apiHost

export default class CaseContainer extends React.Component {
    constructor() {
        super();
        this.columns = [
            {
                Header: "ID",
                accessor: "id",
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "STUDENT ID",
                accessor: "student_id",
                Cell: ({ original }) => {
                    return CaseContainer.renderCell(original.student_id,
                        CaseContainer.isDigit,
                        'Numbers only');
                },
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "TEACHER ID",
                accessor: "teacher_id",
                Cell: ({ original }) => {
                    return CaseContainer.renderCell(original.teacher_id,
                        CaseContainer.isDigit,
                        'Numbers only');
                },
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "CODE",
                accessor: "code",
                Cell: ({ original }) => {
                    return CaseContainer.renderCell(
                        original.code, CaseContainer.validateSelections,
                        'Options not supported',
                        {selections: ['A01', 'A02', 'B02', 'D03']}
                    );
                },
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "DESC",
                accessor: "description",
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "MERIT",
                accessor: "merit",
                Cell: ({ original }) => {
                    return CaseContainer.renderCell(
                        original.merit, CaseContainer.validateMerit,
                        'Must between -100 to 100',
                        {range: [-100, 100]}
                    );
                },
                filterMethod: (filter, row) => {
                    return row[filter.id] <= filter.value;
                },
                Filter: ({ filter, onChange }) => 
                    <div>
                        <div>
                            <input type='number' min="1" max="100" id="merit_range" defaultValue="100"
                                onChange={event => {
                                    onChange(event.target.value);
                                }}
                                style={{ width: '80px' }}
                            />
                        </div>
                        <div>
                            <input type="range" min="-100" max="100" step="1" 
                                value={filter ? filter.value : "100"}
                                style={{ width: "100%" }}
                                name='rangeVal'
                                id='rangeVal'
                                onChange={event => {
                                    onChange(event.target.value);
                                    document.getElementById("merit_range").value = event.target.value;
                                }} />
                        </div>
                    </div>
            },
            {
                Header: "DATE",
                id: 'date',
                accessor: this.formatDate,
                filterMethod: (filter, row) => {
                    let currDate = new Date(moment(row[filter.id], 'DD/MM/YYYY'))
                    console.log(currDate)
                    console.log(this.state.startDate)
                    console.log(this.state.endDate)
                    return currDate >= this.state.startDate && currDate <= this.state.endDate
                },
                Filter: ({ filter, onChange }) => {
                    return (
                        <div>
                            <div>
                                <input type='date'
                                    onChange={event => {
                                        this.setState({ startDate: new Date(event.target.value) });
                                        onChange(event.target.value);
                                    }}
                                />
                            </div>
                            <div>
                                <input type='date'
                                    onChange={event => {
                                        this.setState({ endDate: new Date(event.target.value) });
                                        onChange(event.target.value);
                                    }}
                                />
                            </div>
                        </div>
                    );
                }
            },
            {
                Header: "TIME",
                id: 'time',
                accessor: this.formatTime,
                filterable: false,
            },
        ]
    }

    state = {
        cases: [],
        startDate: this.getMinDate(),
        endDate: this.getMaxDate(),
    }

    componentDidMount() {
        axios.get(`http://${apiHost}/v1/cases`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { cases: res.data } 
                });
            })
    }


    getMinDate() {
        return new Date(-8640000000000000)
    }

    getMaxDate() {
        return new Date(8640000000000000)
    }

    formatDate(data) {
        return moment(data.date).local().format("DD/MM/YYYY");
    }

    formatTime(data) {
        return moment(data.time, "HH:mm").format("hh:mm A");
    }

    static renderCell(input, validator, errorMsg, options={}) {
        if (validator(input, options)) {
            return (
                <div>{input}</div>
            )
        } else {
            return input
            ? (<div data-tip={errorMsg} data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
            : (<div data-tip='Required' data-type='error' style={{ border: '2px dotted #e53051', minHeight: '100%' }}>
                {input}
                <ReactTooltip multiline={true} />
               </div>)
        }
    }

    static validateSelections(input, options) {
        return options.selections.indexOf(input) >= 0;
    }

    static validateMerit(input, options) {
        let num = parseInt(input)
        return (num >= options.range[0] && num <= options.range[1]);
    }


    static validateIC(input) {
        return input.length === 12 && CaseContainer.isDigit(input)
    }

    static isDigit(input) {
        return /^\d+$/.test(input);
    }

    static isAlphabet(input) {
        return /[a-z]/i.test(input);
    }
    render() {
        let user = JSON.parse(localStorage.getItem('user'))
        if (!user) {
            return <Redirect to='/login' />
        } else {
            let decoded = jwt.decode(user.refreshToken)
            let currentTime = Date.now() / 1000
            if (decoded.exp < currentTime) {
                localStorage.removeItem('user')
                return <Redirect to='/login' />
            } else  {
                return (
                    <div>
                        <Header />
                        <Container style={{
                            marginTop: '20px',
                            marginBottom: '20px',
                            minHeight: '800px',
                        }}>
                            <Title isSize={2} style={{
                                textAlign: 'center'
                            }}>{'Cases'}</Title>
                            <Switch>
                                <Route exact path="/cases" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab isActive>
                                                        <Link to='/cases'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab>
                                                        <Link to='/cases/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <ResultPanel columns={this.columns} data={this.state.cases} />
                                        </div>
                                    );
                                }} />
                                <Route path="/cases/edit" render={() => {
                                    return (
                                        <div>
                                            <Tabs isBoxed isSize='medium'>
                                                <TabList>
                                                    <Tab>
                                                        <Link to='/cases'>
                                                            <span>Overview</span>
                                                        </Link>
                                                    </Tab>
                                                    <Tab isActive>
                                                        <Link to='/cases/edit'>
                                                            <span>Edit</span>
                                                        </Link>
                                                    </Tab>
                                                </TabList>
                                            </Tabs>
                                            <DataEntry columns={this.columns} />
                                        </div>
                                    );
                                }} />
                            </Switch>
                        </Container>
                    </div>
                );
            }
        }
    }
}



