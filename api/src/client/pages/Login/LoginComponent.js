import React from 'react';
import { Container, Field, Control, Label, Input, Button, Help } from 'bloomer';

const LoginComponent = (props) => {
    return (
        <Container style={{
            marginTop: '10%',
            maxWidth: '50%',
            border: '2px solid hsl(171, 100%, 41%)',
            borderRadius: '15px'
        }}>
            <div style={{ margin: '5%  5%' }}>
                <Field>
                    <Label>Username</Label>
                    <Control>
                        <Input type="text" placeholder='username' 
                            onChange={(e) => props.handleChange('username', e.target.value)}
                        />
                    </Control>
                </Field>
                <Field>
                    <Label>Password</Label>
                    <Control>
                        <Input type="password" placeholder='********'
                            onChange={(e) => props.handleChange('password', e.target.value)}
                        />
                    </Control>
                    {props.error ? <Help isColor='danger'>Wrong username or password. Try again</Help> : <div></div>}
                </Field>
                <Field isGrouped>
                    <Control>
                        <Button isColor='success'
                            onClick={() => props.handleLogin()}
                        >Login</Button>
                    </Control>
                </Field>
            </div>
        </Container>
    )
}

export default LoginComponent;
