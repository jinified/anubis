import React from 'react';
import Login from './LoginComponent';
import { Container, Title } from 'bloomer';
import { Redirect } from 'react-router-dom';

const axios = require('axios')
const apiHost = process.env.apiHost

export default class LoginContainer extends React.Component {

    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this)
        this.handleLogin = this.handleLogin.bind(this)
    }

    state = {
        username: '',
        password: '',
        redirect: false,
        error: false,
    }

    handleLogin() {
        axios.post(`http://${apiHost}/v1/auth/login`, {
            'username': this.state.username,
            'password': this.state.password
        })
        .then((response) => {
            console.log(response)
            if (response.status === 200) {
                localStorage.setItem('user', JSON.stringify(response.data))
                this.setState({ redirect: true, error: false })
            } 
        })
        .catch((error) => {
            this.setState({ error: true })
            console.log(error);
        });
    }

    handleChange(key, val) {
        this.setState((prevState, props) => {
            let object = {}
            object[key] = val
            return object;
        })
    }

    render() {
        console.log(process.env.apiHost)

        if (this.state.redirect) {
            return (<Redirect to={{
                pathname: '/',
                state: { isAuthenticated: true }
            }} />)
        } else {
            return (
                <Container style={{
                    marginTop: '100px',
                    marginBottom: '20px',
                    minHeight: '800px',
                }}>
                    <Title isSize={2} style={{
                        textAlign: 'center'
                    }}>{'Login'}</Title>
                    <Login
                        username={this.state.username}
                        password={this.state.password}
                        error={this.state.error}
                        handleChange={this.handleChange}
                        handleLogin={this.handleLogin}
                    /> 
                </Container>
            );
        }
    }
}
