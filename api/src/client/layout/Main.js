import React from 'react';
import { Switch, Route } from "react-router-dom";
import Home from '../pages/Home/HomeContainer';
import Student from '../pages/Students/StudentContainer';
import Teacher from '../pages/Teachers/TeacherContainer';
import Case from '../pages/Cases/CaseContainer';
import Login from '../pages/Login/LoginContainer';

export default class MainLayout extends React.Component {

    constructor() {
        super()
    }

    render() {
        return (
        <div>
            <Switch>
                <Route exact path="/" render={({location}) => {
                    let isAuthenticated = location.state ? location.state.isAuthenticated : false
                    return <Home isAuthenticated={isAuthenticated} />
                }} />
                <Route path="/login" render={() => { return <Login /> }} />
                <Route path="/students" component={Student} />
                <Route path="/teachers" component={Teacher} />
                <Route path="/cases" component={Case} />
            </Switch>
        </div>
        );
    }
}



