import types from './types'

const axios = require('axios')

const login = (username, password) => {
    return (dispatch) => {
        dispatch(request({ username }));
        axios.post('http://localhost/auth/login', {
            username: username,
            password: password
        })
        .then( res => { dispatch(success(username)); })
    }

    function request(user) {
        return {
            type: types.LOGIN_REQUEST,
            user
        }
    }

    function success(user) {
        return {
            type: types.LOGIN_SUCCESS,
            user
        }
    }

    function failure(error) {
        return {
            type: types.LOGIN_FAILURE,
            error
        }
    }
};
