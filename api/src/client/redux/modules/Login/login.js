const axios = require('axios')

const LOGIN_REQUEST = 'USERS_LOGIN_REQUEST'
const LOGIN_SUCCESS = 'USERS_LOGIN_SUCCESS'
const LOGIN_FAILURE = 'USERS_LOGIN_FAILURE'

const initialState = {
    isLoggingIn: false,
    error: false
};

export default (state = initialState, action) => {
    switch(action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                isLoggingIn: true
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isLoggingIn: false,
                user: action.result
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                isLoggingIn: false,
                error: action.error
            };
        default:
            return state;
    }
};

export function login(username, password) {
    return {
        types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE],
        promise: () => axios.post('http://localhost/auth/login', {
            username: username,
            password: password
        })
    }
}
