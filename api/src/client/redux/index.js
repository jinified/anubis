import { combineReducers } from 'redux'
import login from './modules/Login/login'

export default combineReducers({
  login,
});
