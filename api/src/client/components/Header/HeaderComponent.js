import React from 'react';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Hero, HeroHeader, Navbar, NavbarMenu, NavbarItem, NavbarEnd, NavbarStart, Title,
         NavbarBrand, Image, NavbarDivider, Container, HeroFooter,
         Tabs, Tab, TabList, TabLink, Icon } from 'bloomer';

// Assets
import darkLogo from './logo_dark.png';

const Header = (props) => {
    return (
        <Hero isColor='primary' isSize='small'>
            <HeroHeader>
                <Navbar style={{ margin: '20px'}}>
                    <Link to='/'>
                    <NavbarBrand> 
                        <Image isSize='48x48' src={darkLogo} / >
                        <Title isSize={3}>{props.brand_name}</Title>
                    </NavbarBrand>
                    </Link>
                    <NavbarMenu>
                        <NavbarEnd style={{
                            fontWeight: 'bold'
                        }}>
                            <Link to={{
                                pathname: '/',
                                state: { isAuthenticated: true }
                            }}><NavbarItem>Home</NavbarItem></Link>
                            <Link to='/students'><NavbarItem>Students</NavbarItem></Link>
                            <Link to='/teachers'><NavbarItem>Teachers</NavbarItem></Link>
                            <Link to='/cases'><NavbarItem>Cases</NavbarItem></Link>
                        </NavbarEnd>
                    </NavbarMenu>
                </Navbar>
            </HeroHeader>
        </Hero>
    );
}


export default Header;
