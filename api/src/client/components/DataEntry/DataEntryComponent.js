import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { CSVLink, CSVDownload } from 'react-csv';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';


const DataEntry = (props) => {
    return (
        <div style={{ 
            marginTop: '30px'
        }}>
            <ReactTable
                data={props.transactions}
                filterable
                defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value
                }
                columns={props.columns}
                defaultSorted={[
                    {
                        id: "id"
                    }
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
             >
                {(state, makeTable, instance) => {
                    return (
                        <div>
                            {makeTable()}
                            <Button isColor='info' isOutlined style={{ marginTop: '20px', marginRight: '20px'}}>Update</Button>
                            <Button isColor='info' isOutlined style={{ marginTop: '20px', marginRight: '20px'}}>Archive</Button>
                            <Button isColor='danger' isOutlined style={{ marginTop: '20px', marginRight: '20px'}}>Delete</Button>
                        </div>
                    );
                }}
            </ReactTable>
        </div>
    );
}

export default DataEntry;
