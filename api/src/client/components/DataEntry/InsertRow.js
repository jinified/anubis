import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { Control, Button, Title, Label, Input, Field } from 'bloomer';

const InsertRow = (props) => {
    return (
    <ReactTable
        data={props.userInput}
        style={{ marginBottom: '20px' }}
        showPagination={false}
        defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) === filter.value
        }
        columns={[
            {
                Header: "ID",
                accessor: "id",
                Cell: props.renderCell['id'](),
                sortMethod: (a, b) => {
                    return parseInt(a) - parseInt(b)
                }
            },
            {
                Header: "IC",
                accessor: "ic",
                Cell: props.renderCell['ic'](),
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().startsWith(filter.value.toUpperCase())
            },
            {
                Header: "Name",
                accessor: "name",
                Cell: props.renderCell['name'](),
                filterMethod: (filter, row) =>
                    row[filter.id].toUpperCase().indexOf(filter.value.toUpperCase()) >= 0
            },
            {
                Header: "CLASS",
                accessor: "class",
                Cell: props.renderCell['class']([
                    ['1CERDAS', '1 Cerdas'],
                    ['1PINTAR', '1 Pintar'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="1CERDAS">1CERDAS</option>
                    </select>
            },
            {
                Header: "GENDER",
                accessor: "gender",
                Cell: props.renderCell['gender']([
                    ['F', 'Female'],
                    ['M', 'Male'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="F">F</option>
                        <option value="M">M</option>
                    </select>
            },
            {
                Header: "RACE",
                accessor: "race",
                Cell: props.renderCell['race']([
                    ['chinese', 'Chinese'],
                    ['malay', 'Malay'],
                    ['indian', 'Indian'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="chinese">Chinese</option>
                        <option value="malay">Malay</option>
                        <option value="indian">Indian</option>
                    </select>
            },
            {
                Header: "RELIGION",
                accessor: "religion",
                Cell: props.renderCell['religion']([
                    ['buddha', 'Buddhism'],
                    ['islam', 'Islam'],
                    ['hindu', 'Hinduism'],
                ]),
                filterMethod: (filter, row) => {
                    if (filter.value === "all") {
                        return true;
                    } else {
                        return row[filter.id] == filter.value;
                    }
                },
                Filter: ({ filter, onChange }) =>
                    <select
                        onChange={event => onChange(event.target.value)}
                        style={{ width: "100%" }}
                        value={filter ? filter.value : "all"}>

                        <option value="all">All</option>
                        <option value="buddha">Buddha</option>
                        <option value="islam">Islam</option>
                        <option value="hindu">Hindu</option>
                    </select>
            },
            {
                Header: "MERIT",
                accessor: "merit",
                Cell: props.renderCell['merit']({
                    min: '1',
                    max: '100',
                    defaultValue: '100'
                }),
                filterMethod: (filter, row) => {
                    return row[filter.id] <= filter.value;
                },
                Filter: ({ filter, onChange }) => 
                    <div>
                        <div>
                            <input type='number' min="1" max="100" id="merit_range" defaultValue="100"
                                onChange={event => {
                                    onChange(event.target.value);
                                }}
                                style={{ width: '80px' }}
                            />
                        </div>
                        <div>
                            <input type="range" min="0" max="100" step="1" 
                                value={filter ? filter.value : "100"}
                                style={{ width: "100%" }}
                                name='rangeVal'
                                id='rangeVal'
                                onChange={event => {
                                    onChange(event.target.value);
                                    document.getElementById("merit_range").value = event.target.value;
                                }} />
                        </div>
                    </div>
            },
        ]}
        defaultPageSize={1}
        className="-striped -highlight"
    >
        {(state, makeTable, instance) => {
            return (
                <div>
                    {makeTable()}
                    <Button isColor='success' style={{ marginBottom: '50px' }}
                        onClick={() => props.handleOnSubmit(state)}
                    >Submit</Button>
                </div>
            );
        }}
    </ReactTable>
    );
}

export default InsertRow;
