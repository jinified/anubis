import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Footer, Container, Columns, Column, Content } from 'bloomer';

const FooterComponent = (props) => {
    return (
        <Footer id='footer'>
            <Container>
                <Content>
                    <Columns>
                        <Column>
                            <p>
                                Made with <FontAwesomeIcon icon='heart' color='red' /> by <b>Jin</b>
                            </p>
                            <a href='mailto:jinified@gmail.com'>jinified@gmail.com</a>
                        </Column>
                    </Columns>
                    <Content isSize='small'>
                        <p>The source code is licensed under <a target="_blank">MIT</a>.</p>
                    </Content>
                </Content>
            </Container>
        </Footer>
    );
}

export default FooterComponent;
