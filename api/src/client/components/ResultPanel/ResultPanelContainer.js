import React from 'react';
import ResultPanel from './ResultPanelComponent';

export default class ResultPanelContainer extends React.Component {
    constructor() {
        super();
        this.setFilters = this.setFilters.bind(this);
        this.toggleRow = this.toggleRow.bind(this);
        this.toggleSelectAll = this.toggleSelectAll.bind(this)
        this.checkSelected = this.checkSelected.bind(this)
        this.checkSelectAll = this.checkSelectAll.bind(this)
        this.setReactTable = this.setReactTable.bind(this)
    }

    state = {
        filtered: [],
        selected: [],
        selectedAll: 0
    }

    setFilters(filtered) {
        this.setState((prevState, props) => { 
            return { filtered }
        })
    }

    toggleRow(id) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[id] = !this.state.selected[id];
        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        let newSelected = {};

        if (this.state.selectAll  === 0) {
            for (let x of this.reactTable.getResolvedState().sortedData) {
                newSelected[x.id] = true;
            }
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    checkSelected(id) {
        return this.state.selected[id] === true;
    }

    checkSelectAll(val) {
        return this.state.selectAll === val;
    }

    setReactTable(r) {
        this.reactTable = r;
    }

    render() {
        return (
            <ResultPanel
                columns={this.props.columns}
                data={this.props.data}
                filtered={this.state.filtered}
                setFilters={this.setFilters}
                toggleSelectAll={this.toggleSelectAll}
                toggleRow={this.toggleRow}
                checkSelected={this.checkSelected}
                checkSelectAll={this.checkSelectAll}
                setReactTable={this.setReactTable}
            />
        );
    }
}

