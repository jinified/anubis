import React from 'react';
import ReactTable from "react-table";
import PropTypes from 'prop-types';
import { CSVLink, CSVDownload } from 'react-csv';
import { Button } from 'bloomer';


const inputStyle = {
    fontSize: '15px',
    padding: '5px',
    borderRadius: '3px',
    border: '1px solid rgba(0,0,0,0.2)',
    marginBottom: '2px',
}

const ResultPanel = (props) => {

    return (
        <div style={{ 
            marginTop: '100px'
        }}>
            <ReactTable
                ref={(r) => props.setReactTable(r)}
                data={props.data}
                filterable
                onFilteredChange={filtered => {props.setFilters(filtered)}}
                filtered={props.filtered}
                defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value
                }
                columns={[{
                    Header: x => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                style={inputStyle}
                                checked={props.checkSelectAll(1)}
                                ref={input => {
                                    if (input) {
                                        input.indeterminate = props.checkSelectAll(2);
                                    }
                                }}
                                onChange={() => props.toggleSelectAll()}
                            />
                        );
                    },
                    id: "checkbox",
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                style={inputStyle}
                                checked={props.checkSelected(original.id)}
                                onChange={() => props.toggleRow(original.id)}
                            />
                        );
                    },
                    filterable: false,
                    sortable: false,
                    width: 45
                }, ...props.columns]}
                defaultSorted={[
                    {
                        id: "id"
                    }
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
             >
                {(state, makeTable, instance) => {
                    return (
                        <div>
                            <Button isColor='primary' style={{ marginBottom: '20px' }}
                                onClick={() => {
                                    props.setFilters([]);
                                    props.toggleSelectAll();
                                }}
                            >Reset filters</Button>
                            {makeTable()}
                            <CSVLink data={state.sortedData.map((row) => {
                                return row._original
                            }).filter((record) => props.checkSelected(record.id))}>
                                <Button isColor='primary' isOutlined style={{ marginTop: '20px' }}>Export as CSV</Button>
                            </CSVLink>
                        </div>
                    );
                }}
            </ReactTable>
        </div>
    );
}

ResultPanel.propTypes = {
    data: PropTypes.array.isRequired
}

export default ResultPanel;
