import React from 'react';
import { Columns, Column, Container, Title, Table } from 'bloomer';
import { VictoryPie, VictoryLegend } from 'victory';



const dataStyle = {
    border: 'solid 1px #ccc',
    minHeight: '500px',
    width: '100%',
    display: 'grid',
}

const titleStyle = {
    textAlign: 'center',
    marginTop: '20px'
}

const Dashboard = (props) => {

    return (
        <div style={{ marginTop: '100px' }}>
        <Columns>
            <Column>
                <Container style={dataStyle}>
                    <Title isSize={4} style={titleStyle}>Cases by Code</Title>
                    <svg width={400} height={300} style={{ margin: 'auto' }}>
                        <VictoryLegend standalone={false}
                            colorScale='qualitative'
                            x={20} y={40}
                            gutter={20}
                            title="Legend"
                            centerTitle
                            data={props.caseByCode.map((record) => { return {name: record.x} })
}
                        />
                        <VictoryPie
                            standalone={false}
                            width={400} height={300}
                            padding={{ 
                                left: 100, bottom: 20, top: 20
                            }}
                            data={props.caseByCode}
                            labelRadius={90}
                            colorScale='qualitative'
                            labels={() => null} 
                        />
                    </svg>
                </Container>
            </Column>
            <Column>
                <Container style={dataStyle}>
                    <Title isSize={4} style={titleStyle}>Cases by Class</Title>
                    <svg width={400} height={300} style={{ margin: 'auto' }}>
                        <VictoryLegend standalone={false}
                            colorScale='qualitative'
                            x={20} y={40}
                            gutter={20}
                            title="Legend"
                            centerTitle
                            data={props.caseByClass.map((record) => { return {name: record.x} })
}
                        />
                        <VictoryPie
                            standalone={false}
                            width={400} height={300}
                            padding={{ 
                                left: 100, bottom: 20, top: 20
                            }}
                            data={props.caseByClass}
                            labelRadius={90}
                            colorScale='qualitative'
                            labels={() => null} 
                        />
                    </svg>
                </Container>
            </Column>
        </Columns>
        <Columns>
            <Column>
                <Container style={dataStyle}>
                    <Title isSize={4} style={titleStyle}>Hall of Shame</Title>
                    <Table isBordered isStriped style={{ margin: 'auto' }}>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Class</th>
                                <th>Merit</th>
                            </tr>
                        </thead>
                    <tbody>
                        {props.hallOfShame.map((student) => {
                            return (
                                <tr>
                                    <td>{student.name}</td>
                                    <td>{student.class}</td>
                                    <td>{student.merit}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                    </Table>
                </Container>
            </Column>
            <Column>
                <Container style={dataStyle}>
                    <Title isSize={4} style={titleStyle}>Hall of Fame</Title>
                    <Table isBordered isStriped style={{ margin: 'auto' }}>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Class</th>
                                <th>Merit</th>
                            </tr>
                        </thead>
                    <tbody>
                        {props.hallOfFame.map((student) => {
                            return (
                                <tr>
                                    <td>{student.name}</td>
                                    <td>{student.class}</td>
                                    <td>{student.merit}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                    </Table>
                </Container>
            </Column>
        </Columns>
        </div>
    )
}

export default Dashboard;
