import React from 'react';
import Dashboard from './DashboardComponent';
const axios = require('axios');
const apiHost = process.env.apiHost

export default class DashboardContainer extends React.Component {
    constructor() {
        super();
    }

    state = {
        students: [],
        teachers: [],
        cases: [],
    }

    componentDidMount() {
        axios.get(`http://${apiHost}/v1/students`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { students: res.data } 
                });
            })
        axios.get(`http://${apiHost}/v1/teachers`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { teachers: res.data } 
                });
            })
        axios.get(`http://${apiHost}/v1/cases`)
            .then(res => {
                this.setState((prevState, props) => {
                    return { cases: res.data } 
                });
            })
    }

    caseByCode() {
        const tally = this.state.cases.reduce( (tally, record) => {
            tally[record.code] = (tally[record.code] || 0) + 1;
            return tally;
        }, {});
        return Array.from(Object.keys(tally), (k) => {
            return {x: k, y: tally[k]};
        });
    }

    caseByClass() {
        const tally = this.state.cases.reduce( (tally, record) => {
            let student = this.state.students.filter(student => { return student.id === record.student_id })[0]
            tally[student.class] = (tally[student.class] || 0) + 1;
            return tally;
        }, {});
        return Array.from(Object.keys(tally), (k) => {
            return {x: k, y: tally[k]};
        });
    }

    hallOfShame(N) {
        const sorted = this.state.students.sort((a, b) => { return a.merit - b.merit }).slice(0, N)
        return sorted.map((student) => {
            return { name: student.name, class: student.class, merit: student.merit };
        });
    }

    hallOfFame(N) {
        const sorted = this.state.students.sort((a, b) => { return b.merit - a.merit }).slice(0, N)
        return sorted.map((student) => {
            return { name: student.name, class: student.class, merit: student.merit };
        });
    }

    render() {
        return (
            <Dashboard
                students={this.state.students}
                teachers={this.state.teachers}
                cases={this.state.cases}
                caseByCode={this.caseByCode()}
                caseByClass={this.caseByClass()}
                hallOfShame={this.hallOfShame(5)}
                hallOfFame={this.hallOfFame(5)}
            />
        )
    }
}
