import React from 'react';
import MainLayout from './layout/Main';
import Header from './components/Header/HeaderContainer';
import Footer from './components/Footer/FooterContainer';

const _ = require('lodash')

// Icon declaration
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faHeart, faTimes } from '@fortawesome/free-solid-svg-icons'
library.add(fab, faHeart, faTimes)


class App extends React.Component {

    render() {
        return (
            <div>
                <MainLayout />
                <Footer />
            </div>
        );
    }
}

export default App;
