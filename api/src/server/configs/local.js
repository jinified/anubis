'use strict';

require('dotenv').config()

let redisConfig = {
    hostname: 'localhost',
    port: 6379
};

let localConfig = {
    hostname: 'localhost',
    port: 5000,
};

let authConfig = {
    secret: process.env.JWTSecret,
    refreshTokenSecret: process.env.JWTRefreshSecret,
    tokenLife: 900,             // 15 min
    refreshTokenLife: 3600,    // 1 hour
    db: process.env.MONGO_URI
}

module.exports = {
    localConfig: localConfig,
    authConfig: authConfig,
    redisConfig: redisConfig
};

