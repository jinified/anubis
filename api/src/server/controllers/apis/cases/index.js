'use strict';

const express = require('express')
const multer = require('multer')
const casesService = require('../../../services/cases')
const BaseJoi = require('joi');
const Extension = require('joi-date-extensions');
const Joi = BaseJoi.extend(Extension);
const validator = require('express-joi-validation')({});
const upload = multer({ dest: 'tmp/csv' });
const { verifyToken, isPermitted } = require('../../../middlewares')

// Schema

const caseSchema = Joi.object({
    student_id: Joi.string().alphanum().required(),
    teacher_id: Joi.string().alphanum().required(),
    merit: Joi.number().integer().required(),
    code: Joi.string().alphanum().required(),
    description: Joi.string().required(),
    date: Joi.date().format('YYYY-MM-DD').required(),
    time: Joi.date().format('HH:mm').required()
})
const arraySchema = Joi.array().items(caseSchema).min(1).unique().required()
const casesSchema = Joi.object({
    cases: Joi.alternatives().try(caseSchema, arraySchema).required()
})
const arrayIdSchema = Joi.array().items(Joi.number().positive().integer()).min(1).unique().required()
// Identify cases by id
const arrayCasesSchema = Joi.object({
    cases: arrayIdSchema
})

let router = express.Router();

router.get('/', casesService.getCases);
router.get('/:id', verifyToken, casesService.getCasesWithId);
router.post('/', validator.body(casesSchema), casesService.createCases);
router.delete('/', validator.body(arrayCasesSchema), casesService.deleteCases);
router.post('/upload-csv', verifyToken, isPermitted('admin'), upload.single('file'), casesService.createCasesWithCSV);

module.exports = router;
