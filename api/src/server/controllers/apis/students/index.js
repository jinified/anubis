'use strict';

const express = require('express')
const Joi = require('joi');
const studentsService = require('../../../services/students')
const validator = require('express-joi-validation')({});
const multer = require('multer')
const upload = multer({ dest: 'tmp/csv' });
const { verifyToken, isPermitted } = require('../../../middlewares')

// Schema

const studentSchema = Joi.object({
    id: Joi.string().alphanum().required(),
    ic: Joi.string().regex(/^[0-9]+$/).length(12).required(),
    name: Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
    gender: Joi.string().length(1).valid('F', 'M').required(),
    race: Joi.string().valid('chinese', 'indian', 'malay').required(),
    religion: Joi.string().valid('buddha', 'islam', 'hindu').required(),
    class: Joi.string().required(),
    merit: Joi.number().integer().required()
})
const arraySchema = Joi.array().items(studentSchema).min(1).unique().required()
const studentsSchema = Joi.object({
    students: Joi.alternatives().try(studentSchema, arraySchema).required()
})
const arrayIdSchema = Joi.array().items(Joi.number().positive().integer()).min(1).unique().required()
// Identify students by id
const arrayStudentsSchema = Joi.object({
    students: arrayIdSchema
})

let router = express.Router();

router.get('/', studentsService.getStudents);
router.get('/:id', verifyToken, studentsService.getStudentsWithId);
router.post('/', validator.body(studentsSchema), studentsService.createStudents);
router.delete('/', validator.body(arrayStudentsSchema), studentsService.deleteStudents);
router.post('/archive', validator.body(arrayStudentsSchema), studentsService.archiveStudents);
router.post('/reset', validator.body(arrayStudentsSchema), studentsService.resetMeritStudents);
router.post('/upload-csv', verifyToken, isPermitted('admin'), upload.single('file'), studentsService.createStudentsWithCSV);

module.exports = router;
