'use strict';

const express = require('express')
const authService = require('../../../services/stats')

let router = express.Router();
router.post('/login', authService.authenticate);
router.post('/register', authService.createUser);
router.post('/token', authService.refreshToken);

module.exports = router;

