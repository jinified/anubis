'use strict';

const express = require('express')
const Joi = require('joi');
const teachersService = require('../../../services/teachers')
const validator = require('express-joi-validation')({});
const multer = require('multer')
const upload = multer({ dest: 'tmp/csv' });
const { verifyToken, isPermitted } = require('../../../middlewares')

const teacherSchema = Joi.object({
    id: Joi.string().alphanum().required(),
    ic: Joi.string().regex(/^[0-9]+$/).length(12).required(),
    name: Joi.string().regex(/^[a-zA-Z\s]+$/).required(),
    class: Joi.string().required(),
    remarks: Joi.string().optional()
});
const arraySchema = Joi.array().items(teacherSchema).min(1).unique().required()
const teachersSchema = Joi.object({
    teachers: Joi.alternatives().try(teacherSchema, arraySchema).required()
})
const arrayIdSchema = Joi.array().items(Joi.number().positive().integer()).min(1).unique().required()
// Identify students by id
const arrayTeachersSchema = Joi.object({
    teachers: arrayIdSchema
})

let router = express.Router();

router.get('/', teachersService.getTeachers);
router.get('/:id', verifyToken, teachersService.getTeachers);
router.post('/', validator.body(teachersSchema), teachersService.createTeachers);
router.delete('/', validator.body(arrayTeachersSchema), teachersService.deleteTeachers);
router.post('/upload-csv', verifyToken, isPermitted('admin'), upload.single('file'), teachersService.createTeachersWithCSV);

module.exports = router;
