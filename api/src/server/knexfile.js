require('dotenv').config()

const pg = require('pg')

module.exports = {
    client: 'pg',
    connection: process.env.DATABASE_URL,

    onUpdateTrigger: table => `
        CREATE TRIGGER ${table}_updated_at
        AFTER UPDATE ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_update_case();
    `,
    onInsertTrigger: table => `
        CREATE TRIGGER ${table}_inserted_at
        AFTER INSERT ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_insert_case();
    `,
    onDeleteTrigger: table => `
        CREATE TRIGGER ${table}_deleted_at
        BEFORE DELETE ON ${table}
        FOR EACH ROW
        EXECUTE PROCEDURE on_delete_case();
    `
}
