const Knex = require('knex')
const connection = require('../knexfile')
const { Model } = require('objection')

const knexConnection = Knex(connection)

Model.knex(knexConnection)

class Cases extends Model {

    static get tableName () {
        return 'cases'
    }

    static get relationMappings () {
        return {
            student: {
                relation: Model.BelongsToOneRelation,
                modelClass: Students,
                join: {
                    from: 'cases.student_id',
                    to: 'students.id'
                }
            },
            teacher: {
                relation: Model.BelongsToOneRelation,
                modelClass: Teachers,
                join: {
                    from: 'cases.teacher_id',
                    to: 'teachers.id'
                }
            }
        }
    }
}

class Students extends Model {

    static get tableName () {
        return 'students'
    }

    static get relationMappings () {
        return {
            cases: {
                relation: Model.HasManyRelation,
                modelClass: Cases,
                join: {
                    from: 'students.id',
                    to: 'cases.student_id'
                }
            }
        }
    }
}

class Teachers extends Model {

    static get tableName () {
        return 'teachers'
    }

    static get relationMappings () {
        return {
            cases: {
                relation: Model.HasManyRelation,
                modelClass: Cases,
                join: {
                    from: 'teachers.id',
                    to: 'cases.teacher_id'
                }
            }
        }
    }
}

module.exports = { Students, Teachers, Cases }
