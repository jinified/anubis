const mongoose = require('mongoose');
const { Schema } = mongoose;

module.exports = mongoose.model('User', new Schema({ 
    username: {
        type: String,
        required: true,
        index: {unique: true, dropDups: true}
    },
    email: {type: String},
    password: {type: String, required: true},
    role: {type: String, default: 'user'}
}));
