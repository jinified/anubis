'use strict';

const apiRoute = require('./apis')
const path = require('path')

function init(server) {
    server.use('/', apiRoute);
    server.get('*', (req,res) =>{
        res.sendFile(path.resolve(__dirname, '../../../dist/index.html'));
    });
}

module.exports = {
    init: init
};
