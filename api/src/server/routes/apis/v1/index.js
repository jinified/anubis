'use strict';

const express = require('express')
const studentsController = require('../../../controllers/apis/students');
const teachersController = require('../../../controllers/apis/teachers');
const casesController = require('../../../controllers/apis/cases');
const authController = require('../../../controllers/apis/auth');

let router = express.Router();

router.use('/students', studentsController);
router.use('/teachers', teachersController);
router.use('/cases', casesController);
router.use('/auth', authController);

module.exports = router;
