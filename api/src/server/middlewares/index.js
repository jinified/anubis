const jwt = require('jsonwebtoken');
const { authConfig } = require('../configs');


function isPermitted(...allowed) {
    const isAllowed = role => allowed.indexOf(role) > -1;

    // return a middleware
    return (req, res, next) => {
        if (isAllowed(req.user.role))
            next(); // role is allowed, so continue on the next middleware
        else {
            res.status(403).send({ error: "Forbidden" }); // user is forbidden
        }
    }
}

function verifyToken(req, res, next) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];

    if (!token) {
        return res.status(403).send({ error: 'No token provided' });
    } else {
        jwt.verify(token, authConfig.secret, function(err, decoded) {
            if (err) {
                return res.status(500).send({ error: 'Failed to authenticate token.' });
            } else {
                req.user = {username: decoded.username, role: decoded.role}
                delete req.query.token;
                next();
            }
        });
    }
}

module.exports = {
    verifyToken: verifyToken,
    isPermitted: isPermitted
}
