'use strict';

const KMS       = require('./utils/kms');
const express   = require('express');
const morgan    = require('morgan');
const mongoose  = require('mongoose');
const jwt       = require('jsonwebtoken');
const helmet    = require('helmet')
const routes    = require('./routes');
const path      = require('path');
const cors      = require('cors')

const server = express()
const { localConfig, authConfig } = require('./configs')

// Server settings
server.use(express.json())
server.use(express.urlencoded({extended: true}))
server.use(morgan('dev'))
server.use(helmet())
server.use(cors())
// Serve the static files from client side app
server.use(express.static(path.resolve(__dirname, '../../dist')));

server.set('env', localConfig.env);
server.set('port', localConfig.port);
server.set('hostname', localConfig.hostname);
// Prettify JSON output
server.set('json spaces', 2);

// Setup DB for authentication
server.set('superSecret', authConfig.secret); // secret variable
console.log("Connecting to mongodb at: " + authConfig.db)
mongoose.connect(authConfig.db, { useNewUrlParser: true }); // connect to database

// Set up routes
routes.init(server);

const hostname = server.get('hostname')
const port = server.get('port');

server.listen(port, function () {
    console.log('Express server listening on - http://' + hostname + ':' + port);
});
