
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('teachers', table => {
            table.string('id').primary();
            table.string('ic');
            table.string('name');
            table.string('class');
            table.string('remarks');
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('teachers')
    ])
};
