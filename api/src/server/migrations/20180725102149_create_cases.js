// Define custom function for trigger
const ON_UPDATE_CASE_FUNCTION = `
    CREATE OR REPLACE FUNCTION on_update_case() RETURNS TRIGGER AS
    $BODY$
    BEGIN
        UPDATE students
        SET merit = merit - OLD.merit + NEW.merit
        WHERE id = OLD.student_id;
        RETURN NEW;
    END;
    $BODY$
    language plpgsql;
`

const ON_INSERT_CASE_FUNCTION = `
    CREATE OR REPLACE FUNCTION on_insert_case() RETURNS TRIGGER AS
    $BODY$
    BEGIN
        UPDATE students
        SET merit = merit + NEW.merit
        WHERE id = NEW.student_id;
        RETURN NEW;
    END;
    $BODY$ 
    language plpgsql;
`

const ON_DELETE_CASE_FUNCTION = `
    CREATE OR REPLACE FUNCTION on_delete_case() RETURNS TRIGGER AS
    $BODY$
    BEGIN
        UPDATE students
        SET merit = merit - OLD.merit
        WHERE id = OLD.student_id;
        RETURN OLD;
    END;
    $BODY$
    language plpgsql;
`


exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('cases', table => {
            table.increments("id").unsigned().primary()
            table.string('student_id').references('id').inTable('students').onDelete('cascade')
            table.string('teacher_id').references('id').inTable('teachers').onDelete('cascade')
            table.integer('merit')
            table.string('code')
            table.string('description')
            table.date('date')
            table.string('time')
            table.timestamps(true, true)
        }),
        knex.raw(ON_UPDATE_CASE_FUNCTION),
        knex.raw(ON_INSERT_CASE_FUNCTION),
        knex.raw(ON_DELETE_CASE_FUNCTION)
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('cases')
    ])
};
