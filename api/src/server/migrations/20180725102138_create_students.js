exports.up = function(knex, Promise) {
    return Promise.all([
        knex.schema.createTable('students', table => {
            table.string('ic', 12)
            table.string('id').primary();
            table.string('name');
            table.string('gender');
            table.string('race');
            table.string('religion');
            table.string('class');
            table.integer('merit');
        })
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('students')
    ])
};
