
const { onUpdateTrigger, onInsertTrigger, onDeleteTrigger } = require('../knexfile')
exports.up = function(knex, Promise) {
    return Promise.all([
        knex.raw(onUpdateTrigger('cases')),
        knex.raw(onInsertTrigger('cases')),
        knex.raw(onDeleteTrigger('cases'))
    ])
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTableIfExists('cases')
    ])
};
