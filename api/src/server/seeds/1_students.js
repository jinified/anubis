
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('students').del()
    .then(function () {
      // Inserts seed entries
      return knex('students').insert([
        {id: "1", ic: "050707070834", name: "NUR FARIZATUL AISHAH BINTI ABDUL GHAFFAR", gender: "F", race: "malay", religion: "islam", class: "1CERDAS", merit: 100},
        {id: "2", ic: "040730070307", name: "YEAP KHAI LOON", gender: "M", race: "chinese", religion: "buddha", class: "1PINTAR", merit: 100},
        {id: "3", ic: "041211070721", name: "OH ZHONG KEN", gender: "M", race: "chinese", religion: "buddha", class: "1PINTAR", merit: 100},
        {id: "4", ic: "050105070407", name: "ABDUL QAHHAR BIN MOHD AZHAR", gender: "M", race: "malay", religion: "islam", class: "1CERDAS", merit: 100},
        {id: "5", ic: "050505070553", name: "SHRIEMAAN RAAJ A/L ISVARAN", gender: "M", race: "indian", religion: "hindu", class: "1CERDAS", merit: 100},
        {id: "6", ic: "040108020959", name: "KERSHAN A/L OMYANDRAN", gender: "M", race: "indian", religion: "hindu", class: "1TEKUN", merit: 100},
        {id: "7", ic: "041202070535", name: "TEOH TIAN AIK", gender: "M", race: "chinese", religion: "buddha", class: "1CERDAS", merit: 100},
        {id: "8", ic: "041223070761", name: "AVINAASH A/L SARVESWAREN", gender: "M", race: "indian", religion: "hindu", class: "1CERDAS", merit: 100},
        {id: "9", ic: "051107070413", name: "MUHAMMAD ASMAWI ANIQ BIN MOHD RADZI", gender: "M", race: "malay", religion: "islam", class: "1CERDAS", merit: 100},
        {id: "10", ic: "050329070086", name: "NURIN BATRISYIA BINTI AZLI", gender: "F", race: "malay", religion: "islam", class: "1CERDAS", merit: 100}
      ]);
    });
};
