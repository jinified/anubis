exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('cases').del()
    .then(function () {
      // Inserts seed entries
      return knex('cases').insert([
        {student_id: "1", teacher_id: "2", code: "A01", description: "Punch the wall", merit: -10, date: "2018-03-01", time: "14:00"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "3", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "14:10"},
        {student_id: "2", teacher_id: "3", code: "B02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "D03", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "B02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "4", teacher_id: "3", code: "B02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "B02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "12:10"},
        {student_id: "5", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "B02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A01", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "4", teacher_id: "3", code: "A01", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "3", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "1", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
        {student_id: "2", teacher_id: "3", code: "A02", description: "Punch the boy", merit: -20, date: "2018-04-01", time: "09:10"},
      ]);
    });
};
