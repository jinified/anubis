
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('teachers').del()
    .then(function () {
      // Inserts seed entries
      return knex('teachers').insert([
        {id: "1", ic: "651210079113", name: "John Doe", class: "1CERDAS"},
        {id: "2", ic: "711022103344", name: "Ahmad Syafiq", class: "1CERDAS"},
        {id: "3", ic: "811004045352", name: "David Chng Yeah Soon", class: "1CERDAS"},
        {id: "4", ic: "900512091122", name: "Gloria Manakam", class: "1CERDAS"},
        {id: "5", ic: "931109075331", name: "Chang Mei Foong", class: "1CERDAS"},
        {id: "6", ic: "850718121155", name: "Sue Ann", class: "1CERDAS"}
      ]);
    });
};
