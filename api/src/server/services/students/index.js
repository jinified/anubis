'use strict';

const express = require('express')
const findQuery = require('objection-find')
const { Students } = require('../../models/schema')
const csvParser = require('fast-csv')
const fs = require('fs')
const csv = require('csv-express')

async function getStudents(req, res, next) {
    console.log('Obtain all students info')
    try {
        let is_export = req.query.is_export || false
        delete req.query['is_export']
        const result = await findQuery(Students)
            .build(req.query)
        if (is_export) {
            res.csv(result, true, {'Content-Disposition': 'attachment; filename=result-students.csv'})
        } else {
            res.json(result)
        }
    } catch (err) {
        console.log(err)
        res.status(400).send("Invalid query parameter(s)")
    }
}


async function createStudents(req, res) {
    console.log('Add new student(s)')

    const options = {
        noDelete: true,
        insertMissing: true
    };

    try {
        const inserted = await Students.query().upsertGraph(req.body.students, options)
        res.json(inserted)
    } catch (err) {
        console.log(err)
        res.status(409).send("Failed to create/update students")
    }
}

async function createStudentsWithCSV(req, res) {
    console.log('Add new student(s) with CSV')

    const options = {
        noDelete: true,
        insertMissing: true
    };
    const fileRows = [];

    try {
        csvParser.fromPath(req.file.path, {headers: true})
            .on("data", (data) => {
                fileRows.push(data); // push each row
            })
            .on("end", async () => {
                fs.unlinkSync(req.file.path);   // remove temp file
                const inserted = await Students.query().upsertGraph(fileRows, options)
                res.json(inserted)
            })
    } catch (err) {
        console.log(err)
        res.status(400).send("Failed to upload file")
    }
}

async function deleteStudents(req, res) {
    console.log('Delete student(s) with id: ' + req.body.students)
    try {
        const deleted = await Students.query().findByIds(req.body.students).delete()
        res.send(deleted + " record(s) deleted")
    } catch (err) {
        res.status(409).send("Failed to delete students")
    }
}

async function archiveStudents(req, res) {
    console.log('Archive student(s) with id: ' + req.body.students)
    try {
        const archived = await Students.query().patch({class: 'ARCHIVE'}).where('id', 'in', req.body.students)
        res.send(archived + " record(s) archived")
    } catch (err) {
        res.status(409).send("Failed to archive students")
    }
}

async function resetMeritStudents(req, res) {
    console.log('Reset student(s) merit with id: ' + req.body.students)
    try {
        const reset = await Students.query().patch({merit: 100}).where('id', 'in', req.body.students)
        res.send(reset + " record(s) has been reset")
    } catch (err) {
        res.status(409).send("Failed to reset students")
    }
}

async function getStudentsWithId(req, res) {
    try {
        const result = await Students.query().findById(req.params.id).eager('cases')
        res.json(result)
    } catch (err) {
        res.status(404).send("No record found")
    }
}

module.exports = {
    getStudents: getStudents,
    getStudentsWithId: getStudentsWithId,
    createStudents: createStudents,
    createStudentsWithCSV: createStudentsWithCSV,
    deleteStudents: deleteStudents,
    archiveStudents: archiveStudents,
    resetMeritStudents: resetMeritStudents
};

