'use strict';

const express = require('express')
const findQuery = require('objection-find')
const { Teachers } = require('../../models/schema')
const csvParser = require('fast-csv')
const fs = require('fs')
const csv = require('csv-express')

async function getTeachers(req, res) {
    console.log('Obtain all cases info')
    try {
        let is_export = req.query.is_export || false
        delete req.query['is_export']
        const result = await findQuery(Teachers)
            .build(req.query)
        if (is_export) {
            res.csv(result, true, {'Content-Disposition': 'attachment; filename=result-teachers.csv'})
        } else {
            res.json(result)
        }
    } catch (err) {
        res.status(400).send("Invalid query parameter(s)")
    }
}

async function createTeachers(req, res) {
    const options = {
        noDelete: true,
        insertMissing: true
    };

    try {
        const inserted = await Teachers.query().upsertGraph(req.body.teachers, options)
        res.json(inserted)
    } catch (err) {
        console.log(err)
        res.status(409).send("Failed to create/update teachers")
    }
}

async function createTeachersWithCSV(req, res) {
    console.log('Add new teacher(s) with CSV')

    const options = {
        noDelete: true,
        insertMissing: true
    };
    const fileRows = [];

    try {
        csvParser.fromPath(req.file.path, {headers: true})
            .on("data", (data) => {
                fileRows.push(data); // push each row
            })
            .on("end", async () => {
                fs.unlinkSync(req.file.path);   // remove temp file
                const inserted = await Teachers.query().upsertGraph(fileRows, options)
                res.json(inserted)
            })
    } catch (err) {
        console.log(err)
        res.status(400).send("Failed to upload file")
    }
}

async function deleteTeachers(req, res) {
    console.log('Delete teacher(s) with id: ' + req.body.teachers)
    try {
        const deleted = await Teachers.query().findByIds(req.body.teachers).delete()
        res.send(deleted + "record(s) deleted")
    } catch (err) {
        res.status(409).send("Failed to delete teachers")
    }
}

async function getTeachersWithId(req, res) {
    try {
        const result = await Teachers.query().findById(req.params.id)
        res.json(result)
    } catch (err) {
        res.status(404).send("No record found")
    }
}

module.exports = {
    getTeachers: getTeachers,
    getTeachersWithId: getTeachersWithId,
    createTeachers: createTeachers,
    createTeachersWithCSV: createTeachersWithCSV,
    deleteTeachers: deleteTeachers
};

