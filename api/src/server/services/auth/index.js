'use strict';

const express = require('express')
const { authConfig } = require('../../configs')
const User = require('../../models/user')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
const KMS = require('../../utils/kms')

async function refreshToken(req, res) {
    const postData = req.body
    if ((postData.refreshToken) && (KMS.client.exists(postData.refreshToken))) {
        const payload = {
            username: postData.username,
            role: postData.role
        }
        const token = jwt.sign(payload, authConfig.secret, { expiresIn: authConfig.tokenLife })
        const response = {
            message: "Here is your newly created access token",
            token: token
        }
        // update the token in the list
        KMS.updateToken(postData.refreshToken, token)
        res.status(200).json(response);
    } else {
        res.status(404).send('Invalid request')
    }
}

async function createUser(req, res) {
    const hashedPassword = await bcrypt.hash(req.body.password, 10)
    User.find({username: req.body.username}, (err, docs) => {
        if (docs.length) {
            res.status(409).send({error: 'Username already exists'})
        } else {
            const user = User.create({ 
                username : req.body.username,
                email : req.body.email || '',
                password : hashedPassword,
                role: req.body.role || 'user'
            }, (err, user) => {
                if (err) {
                    console.log(err)
                    res.status(500).send("There was a problem registering a user")
                } else {
                    const payload = { 
                        username: user.username,
                        role: user.role,
                    };
                    // create access token
                    const token = jwt.sign(payload, authConfig.secret, {
                        expiresIn: authConfig.tokenLife
                    });
                    // create refresh token
                    const refreshToken = jwt.sign(payload, authConfig.refreshTokenSecret, {
                        expiresIn: authConfig.refreshTokenLife
                    });
                    const response = {
                        access_token: token,
                        refresh_token: refreshToken
                    }
                    KMS.addToken(refreshToken, token)
                    res.status(200).send(response)
                }
            });
        }
    });
}

async function authenticate(req, res) {
    User.findOne({
        username: req.body.username
    }, async function(err, user) {

        if (err) throw err;

        if (!user) {
            res.status(401).send({error: 'Wrong username/password'})
        } else if (user) {
            // check if password matches
            const is_match = await bcrypt.compare(req.body.password, user.password);
            if (is_match) {
            // if user is found and password is right
            // create a token with only our given payload
            // we don't want to pass in the entire user since that has the password
                const payload = {
                    username: user.username,
                    role: user.role
                };
                const token = jwt.sign(payload, authConfig.secret, {
                    expiresIn: authConfig.tokenLife
                });
                const refreshToken = jwt.sign(payload, authConfig.refreshTokenSecret, {
                    expiresIn: authConfig.refreshTokenLife
                });

            // return the information including token as JSON
                res.json({
                    auth: true,
                    token: token,
                    refreshToken: refreshToken
                });
            } else {
                res.status(401).send({error: 'Wrong username/password'})
            }   
        }
    });
}

module.exports = {
    authenticate: authenticate,
    createUser: createUser,
    refreshToken: refreshToken
};

