'use strict';

const express = require('express')
const findQuery = require('objection-find')
const { Cases } = require('../../models/schema')
const multer = require('multer');
const csvParser = require('fast-csv')
const fs = require('fs')
const csv = require('csv-express')


async function getCases(req, res) {
    console.log('Obtain all cases info')
    try {
        let is_export = req.query.is_export || false
        delete req.query['is_export']
        const result = await findQuery(Cases)
            .build(req.query)
        if (is_export) {
            res.csv(result, true, {'Content-Disposition': 'attachment; filename=result-cases.csv'})
        } else {
            res.json(result)
        }
    } catch (err) {
        res.status(400).send("Invalid query parameter(s)")
    }
}

async function createCases(req, res) {
    console.log('Add new case(s)')

    const options = {
        noDelete: true
    };

    try {
        const inserted = await Cases.query().upsertGraph(req.body.cases, options)
        res.json(inserted)
    } catch (err) {
        console.log(err)
        res.status(409).send("Failed to create/update cases")
    }
}

async function createCasesWithCSV(req, res) {
    console.log('Add new case(s) with CSV')

    const options = {
        noDelete: true
    };
    const fileRows = [];

    try {
        csvParser.fromPath(req.file.path, {headers: true})
            .on("data", (data) => {
                fileRows.push(data); // push each row
            })
            .on("end", async () => {
                fs.unlinkSync(req.file.path);   // remove temp file
                const inserted = await Cases.query().upsertGraph(fileRows, options)
                res.json(inserted)
            })
    } catch (err) {
        console.log(err)
        res.status(400).send("Failed to upload file")
    }
}

async function deleteCases(req, res) {
    console.log('Delete case(s) with id: ' + req.body.cases)
    try {
        const deleted = await Cases.query().findByIds(req.body.cases).delete()
        res.send(deleted + "record(s) deleted")
    } catch (err) {
        res.status(409).send("Failed to delete cases\n" + err)
    }
}

async function getCasesWithId(req, res) {
    try {
        const result = await Cases.query().findById(req.params.id)
        res.json(result)
    } catch (err) {
        res.status(404).send("No record found")
    }
}

module.exports = {
    getCases: getCases,
    getCasesWithId: getCasesWithId,
    createCases: createCases,
    createCasesWithCSV: createCasesWithCSV,
    deleteCases: deleteCases
};

