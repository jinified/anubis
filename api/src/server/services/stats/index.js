'use strict';

const express = require('express')

module.exports = {
    caseByCode: caseByCode,
    caseByClass: caseByClass,
    caseByDay: caseByDay,
    hallOfShame: hallOfShame,
    hallOfFame: hallOfFame,
};

