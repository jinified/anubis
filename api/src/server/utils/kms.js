// Key-management service

const bluebird          = require('bluebird')
const redis             = require('redis')
const { redisConfig, authConfig }   = require('../configs')

// Promisify redis
bluebird.promisifyAll(redis.RedisClient.prototype);

class KMS {

    constructor (config) {
        this.client = redis.createClient(config.port, config.hostname);
        this.client.on('error', function(err) {
            console.log(err)
        });
        this.client.on('connect', function() {
            console.log('Redis connected.');
        });
        console.log('Connecting to Redis...'); 
    }

    addToken(key, token) {
        this.client.set(key, token, 'EX', authConfig.refreshTokenLife, (err, res) => {
            if (err) throw err;
            console.log("Successfully added refresh token")
        }) 
    }

    updateToken(key, token) {
        this.client.set(key, token, (err, res) => {
            if (err) throw err;
            console.log("Successfully updated access token")
        }) 
    }
}

module.exports = new KMS(redisConfig.port, redisConfig.hostname);
