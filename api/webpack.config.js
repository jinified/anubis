const path = require("path");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack')

const outputDirectory = "dist/";
const publicDirectory = process.env.PUBLIC_PATH || '/';

const devAPIURL = 'localhost'
const productionAPIURL = '35.231.87.9:5000'

const setupApiEnv = (env) => {
    console.log(env)
    if (env === 'development') {
        return devAPIURL
    } else if (env === 'production') {
        return productionAPIURL
    } else {
        return ''
    }
}

module.exports = (env, argv) => {
    return {
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: true // set to true if you want JS source maps
                }),
                new OptimizeCSSAssetsPlugin({})
            ]
        },
        entry: "./src/client/index.js",
        output: {
            path: path.join(__dirname, outputDirectory),
             publicPath: '/',
            filename: "bundle.js",
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader"
                    }
                },
                {
                    test: /\.css$/,
                    use: [
                        "style-loader", 
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: true,
                                modules: true,
                                localIdentName: "[local]___[hash:base64:5]"
                            }
                        }
                    ]
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: '[name]-[hash].[ext]',
                                outputPath: 'fonts/'
                            }
                        },
                    ]
                },
                {
                    test: /\.(png|svg|jp(e*)g)$/,
                    use: [
                        {
                            loader: "url-loader",
                            options: {
                                limit: 8000,
                                name: '[name]-[hash].[ext]',
                                outputPath: 'images/'
                            }
                        },
                    ]
                },
                {
                    test: /\.(scss|sass)$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: "css-loader",
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                includePaths: [path.resolve(__dirname, 'node_modules')]
                            }
                        }
                    ]
                }
            ]
        },
        devServer: {
            port: 80,
            open: true,
            proxy: {
                "/v1": "http://localhost:5000"
            },
            historyApiFallback: true
        },
        plugins: [
            new CleanWebpackPlugin([outputDirectory]),
            new HtmlWebpackPlugin({
                template: "./public/index.html",
                favicon: "./public/favicon.ico"
            }),
            new MiniCssExtractPlugin({
                filename: "style.[chunkhash].css",
            }),
            new CopyWebpackPlugin([
                { from: 'src/client/assets', to: 'assets' }
            ]),
            new webpack.DefinePlugin({
                'process.env.apiHost': JSON.stringify(setupApiEnv(argv.mode))
            })
        ]
    }
};
