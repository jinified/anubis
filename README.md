# [Anubis: Student Discipline Management System](https://bitbucket.org/jinified/anubis/src/master/) ![logo](static/logo_master_small.png)


## Problems

- specific setup is needed (DOS emulator)
- unable to support concurrent write to the system
- must input case on individually
- unable to perform search / custom filtering

## Requirements

### Authentication & Authorization System

- as an admin, I can make changes to existing database
- as a normal user, I have read-only access to the system

### Student Management System

- as a teacher, I want to add student(s) individually or import from csv file
- as a teacher, I want to edit student(s) individually or import from csv file
- as a teacher, I want to delete student(s) individually or import from csv file
- as a teacher, I want to reset student(s) merit individually or import from csv file
- as a teacher, I want to archive student(s) individually or import from csv file
- as a teacher, I want to search student records with **[student id | ic | name]**
    - as a teacher, I want to filter result by **[class, race, religion, gender, merit]**
    - as a teacher, I want to export filter result to csv format
    - as a teacher, I want to **[delete | reset]** filter result

### Teacher Management System

- as an admin, I want to add teacher(s) individually or import from csv file
- as an admin, I want to edit teacher(s) individually or import from csv file
- as an admin, I want to delete student(s) individually or import from csv file
- as an admin, I want to search student records with **[teacher id | name]**
    - as an admin, I want to filter result by **class**
    - as an admin, I want to export filter result to csv format
    - as an admin, I want to **delete** filter result

### Case Management System

- as a teacher, I want to add case(s) individually or import from csv file
    - as a teacher, I should be able to undo addition of case(s)
- as a teacher, I want to edit case(s) individually or import from csv file
- as a teacher, I want to delete case(s) individually or import from csv file
- as a teacher, I want to search student records with **case id**
    - as a teacher, I want to filter result by **[date, time, case category id, reporter, student id, student i.c, student name]**
    - as a teacher, I want to export filter result to csv format
    - as a teacher, I want to **[delete | reset]** filter result

## Non-functional requirements

- any operation should take a maximum of 5 seconds to complete
- application is accessible with only a browser

---

## Milestones

### Must Have

- designed API specs for all supported features
- migrated existing data to database
- tested all features using curl
- completed authentication and authorization system
- designed user journey, color palette, logo
- completed landing page
- completed home page
- completed case management page
- completed student management page
- completed teacher management page

### Good to Have

- completed basic dashboard
- forecast future discipline problems
- send monthly report to email address
- drag and drop csv file
- ocr to scan discipline form

---

## Schema

### Student

- student id (primary key): used locally by the school to identify each student 
- i.c (primary key):
- name
- gender
- race
- religion
- class
- merit

### Teacher

- teacher id (primary key)
- name
- remarks
- class

### Case

- case id (primary key)
- student id
- teacher id
- merit
- case category code
- case description
- time
- date
- remarks

---

## Technical Architecture

### API Design

- OpenAPI

### Frontend

- react pwa
- sass

### Backend

- Node.js
- Hapi.js
- Sqlite

### DevOps

- SSL encrypted
- Run as docker container


---

## Issues

- simultaneous update problem
- record case with multiple students involved
- migrate existing information to new format

---

## Setup API server

- write OpenAPI 3.0 spec using swagger editor
- generate express code from spec using `swagger-node-codegen`
- [better express routing and architecture](https://www.caffeinecoding.com/better-express-routing-for-nodejs/)
    - [project example](https://github.com/kelyvin/express-env-example)
- [best practices for express app structure](https://www.terlici.com/2014/08/25/best-practices-express-structure.html)
- [Creating user, database and adding access on PostgreSQL](https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e)
- [name your seed file in order you want it to be executed](https://stackoverflow.com/questions/42883237/my-seed-runs-but-i-can-not-see-the-data-on-my-table)
- [JSON body validation before processing request](http://evanshortiss.com/development/nodejs/express/2017/04/24/express-joi.html)
- [Handling multiple object in json as POST request](https://stackoverflow.com/questions/35488386/post-multiple-json-objects-simultaneously-with-express-and-postman)
- [CSV file handling for Node endpoint](https://www.techighness.com/post/node-expressjs-endpoint-to-upload-and-process-csv-file/)
- [Running on port 80 to prevent entering port number in url](https://stackoverflow.com/questions/16573668/best-practices-when-running-node-js-with-port-80-ubuntu-linode)
    - `sudo apt-get install libcap2-bin && sudo setcap cap_net_bind_service=+ep $(readlink -f $(which node))`
- [Authenticate a NodeJS API with JWT](https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens)
    - setup mongodb `sudo apt-get install mongodb && sudo mkdir -p /data/db; mongod`
    - [User authentication for mongodb](https://docs.mongodb.com/manual/tutorial/enable-authentication/)
    - Running mongod with authentication enabled `mongod --auth --port 27017 --dbpath /data/db`
    - [invalidating JWT](https://stackoverflow.com/questions/21978658/invalidating-json-web-tokens)
- [Understanding OAuth](https://www.oauth.com)
- [refresh jwt token authentication](https://codeforgeek.com/2018/03/refresh-token-jwt-nodejs-authentication/)
- [Securing Node.js RESTful APIs with JWT](https://medium.freecodecamp.org/securing-node-js-restful-apis-with-json-web-tokens-9f811a92bb52)
- [Using Loopback to build RESTful authentication service](https://medium.freecodecamp.org/build-restful-api-with-authentication-under-5-minutes-using-loopback-by-expressjs-no-programming-31231b8472ca)
- [OAuth server](https://github.com/auth0-blog/blog-refresh-tokens-sample)
- [How to perform token revocation](https://stackoverflow.com/questions/44280736/json-web-token-expiration)
- [Using redis database to store refresh tokens](https://medium.com/@tarekyehia/node-js-oauth-2-server-using-redis-database-f9899f5f1630)
- OAuth useful tools
    - [express-jwt](https://github.com/auth0/express-jwt)
    - [express-jwt-permission](https://github.com/MichielDeMey/express-jwt-permissions)
- [Why do we need access token and refresh token](https://stackoverflow.com/questions/3487991/why-does-oauth-v2-have-both-access-and-refresh-tokens)
- [10 things about token vs cookies](https://auth0.com/blog/ten-things-you-should-know-about-tokens-and-cookies//)
- [Role-based authentication](https://arthur.gonigberg.com/2013/06/29/angularjs-role-based-auth/)
- [Authentication and Authorization with Hapi](https://medium.com/@poeticninja/authentication-and-authorization-with-hapi-5529b5ecc8ec)
- [Express.js role based permissions middleware](https://gist.github.com/joshnuss/37ebaf958fe65a18d4ff)
- [Using redis to handle session in Nodejs](https://codeforgeek.com/2015/07/using-redis-to-handle-session-in-node-js/)
    - `sudo apt-get install redis-server`
- [Use Let's Encrypt with Express](https://medium.com/@yash.kulshrestha/using-lets-encrypt-with-express-e069c7abe625)
- [Express + Node + LetsEncrypt](https://itnext.io/node-express-letsencrypt-generate-a-free-ssl-certificate-and-run-an-https-server-in-5-minutes-a730fbe528ca)


### Objection.js

- [Objection.js - London Node User Group 2018](https://www.youtube.com/watch?v=RyQ1MTVjYK8)
    - create model with ES6 classes
    - make queries with async/wait 
    - eager loading 
    - using knex migration (up and down) to track changes to your schema over time
    - define relationship between table 
    - lifecycle function
    - performing validation with json schema
    - objection-password
- [Objection-find](https://github.com/Vincit/objection-find)

- Swagger to generate documentation from yaml file
- Combination of Knex + Objection.js
- [Objection + Knex = Painless PostgreSQL in Node App](https://dev.to/aspittel/objection--knex--painless-postgresql-in-your-node-app--6n6)
- 


---

## Front-End

- [Full stack web application using React, Nodejs and Webpack](https://hackernoon.com/full-stack-web-application-using-react-node-js-express-and-webpack-97dbd5b9d708)
- [Build a realtime PWA with React](https://medium.com/front-end-hacking/build-a-realtime-pwa-with-react-99e7b0fd3270)
    - [Service Worker API](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API) 
    - [Offline cookbook](https://developers.google.com/web/fundamentals/instant-and-offline/offline-cookbook/)
    - [localForage: wrapper around WebSQL, IndexedDB and localStorage](https://github.com/localForage/localForage)
    - Add manifest.json

### UI Framework to go with React

- Semantic-UI with React-widget for more specialized needs
    - menu won't hide properly on mobile
    - not in development for a while
    - quite easily customizable
- Material-UI: good for small app. quite limited. no good buggy
- [Bulma Coding Walkthrough](https://www.youtube.com/watch?v=MchBPICewgs)
- [Bulma CSS Framework Crash Course](https://www.youtube.com/watch?v=IiPQYQT2-wg)
- [Ant-design](https://ant.design/docs/react/getting-started)
- AtlasKit
- ReBulma
- React-strap
- [Reacting responsively](https://codeburst.io/reacting-responsively-d81812d1e7d)
- [Building a website with react and bulma](https://hackernoon.com/building-a-website-with-react-and-bulma-d655214bff2a)
- Decided on using [bloomer](https://github.com/AlgusDark/bloomer) / [react-bulma-components](https://github.com/couds/react-bulma-components)

### React Patterns & Best Practices

- [Container Component](https://medium.com/@learnreact/container-components-c0e67432e005)
    - separate data fetching and presentation logic
- [Evolving patterns in react](https://medium.freecodecamp.org/evolving-patterns-in-react-116140e5fe8f)
    - destructuring props and use spread
    - provider pattern: pass down props to multiple components
    - higher order component
    - [Do more with less using render prop](https://hackernoon.com/do-more-with-less-using-render-props-de5bcdfbe74c)
- [React patterns you should know](https://medium.com/@joomiguelcunha/react-patterns-you-should-know-da86568372fa)
    - Using PropType to enforce type of props expected
    - Using [children prop](https://codeburst.io/a-quick-intro-to-reacts-props-children-cb3d2fce4891)
    - using conditional rendering
- [reactpattern](https://reactpatterns.com)
    - use state hoisting by passing callback down to children
- [Code Reuse Pattern - Guy Romm](https://www.youtube.com/watch?v=0BNgi9vofaw)
    - Simple composition using prop.children. Extract out common behaviour and wrap around intended class
    - (Higher Order Function) generator function that takes in a Inner Component
    - Happens statically for all HoC
- [Michael Jackson - Never Write Another HoC](https://www.youtube.com/watch?v=BcVAq3YFiuc)
- [Thinking in React](https://reactjs.org/docs/thinking-in-react.html)
- [Understand how container and presentational component](https://scotch.io/courses/5-essential-react-concepts-to-know-before-learning-redux/presentational-and-container-component-pattern-in-react)
- [React anti-pattern](https://codeburst.io/how-to-not-react-common-anti-patterns-and-gotchas-in-react-40141fe0dcd)
- [Why you need keys for collection in React](https://paulgray.net/keys-in-react/)

### React Organizing

- [Writing Scalable React App with Component Folder](https://medium.com/styled-components/component-folder-pattern-ee42df37ec68)
- [100% correct way to organize](https://hackernoon.com/the-100-correct-way-to-structure-a-react-app-or-why-theres-no-such-thing-3ede534ef1ed)
    - Having fully qualified name: HeaderNav instead of Nav in Header directory
    - wrap header container around components
    - self-contained component with own css and test. using css modules
- [Structuring react app](https://hackernoon.com/structuring-projects-and-naming-components-in-react-1261b6e18d76)
    - group the files by module/feature
- [How to structure your react dave ceddia](https://daveceddia.com/react-project-structure/)
    - separate to containers and components folder
- [Structure react-redux for scalibility](https://levelup.gitconnected.com/structure-your-react-redux-project-for-scalability-and-maintainability-618ad82e32b7)
    - organize by view then components and containers in each view
    - one duck directory with types, actions (using reduxsauce)
    - common folder to share between features
- [Fractal pattern](https://hackernoon.com/fractal-a-react-app-structure-for-infinite-scale-4dab943092af)
    - pages: root level component
    - modules: handles state
    - components: shared components like Button which is required by more than one component
    - utils
    - All components are CamelCased
    - All nodes are lowerCased
- [React-redux scaling with fractal](https://medium.com/@hankchiu.tw/react-redux-scaling-with-presentational-and-container-components-in-fractal-structure-4d5b9b12cc94<Paste>)
	- 
- [How to structure react app hackernoon](https://hackernoon.com/how-to-structure-your-react-app-98c48e102aad)
    - common bundle
    - business bundle
        - index.js to reexport 
- [How to better organize you React application](https://medium.com/@alexmngn/how-to-better-organize-your-react-applications-2fd3ea1920f1)
- [Structuring a React App for a CMS](https://x-team.com/blog/react-structure-cms/)
- [React Bits](https://legacy.gitbook.com/book/vasanthk/react-bits/details)


### React Router

- [Pusher: Getting Started with React Router v4](https://blog.pusher.com/getting-started-with-react-router-v4/)
    - import `react-router-dom`. Typically uses import { BrowserRouter as Router } from react-router-dom
    - using switch with route
- [React router practical tutorial](https://github.com/auth0/blog/blob/master/_posts/2018-03-27-react-router-4-practical-tutorial.markdown)
- [Scotch React Router Guide](https://scotch.io/tutorials/routing-react-apps-the-complete-guide)

### Babel

- [using proptypes with transform class properties](https://engineering.musefind.com/our-best-practices-for-writing-react-components-dec3eb5c3fc8)

### CSS

- [CSS-Trick Using CSS Module Part 3](https://css-tricks.com/css-modules-part-3-react/)
- [Evolution from CSS, SASS, CSS Module and Styled-component](https://medium.com/@perezpriego7/css-evolution-from-css-sass-bem-css-modules-to-styled-components-d4c1da3a659b)

### Webpack

- [A tale of Webpack 4 and how to finally configure it the right way](https://hackernoon.com/a-tale-of-webpack-4-and-how-to-finally-configure-it-in-the-right-way-4e94c8e7e5c1)

- [Building website with Bulma and React](https://hackernoon.com/building-a-website-with-react-and-bulma-d655214bff2a)

### Redux Best Practices

- [Scaling your redux app with duck](https://medium.freecodecamp.org/scaling-your-redux-app-with-ducks-6115955638be)


- [Cheng Lou - On the Spectrum of Abstraction at react-europe 2016](https://www.youtube.com/watch?v=mVVNJKv9esE)
---

## Resources

### Project

- [Current Discipline Management Software](https://drive.google.com/open?id=0BwPtfN2OHYOWVmlEM3g2R2NDUmc)
- [DOS Box](https://sourceforge.net/projects/dosbox/files/dosbox/0.74/DOSBox0.74-win32-installer.exe/download)

### Tools

- [Coolors.co: Color Palette Generator](https://coolors.co/app)

### Tutorials

#### Authentication

- [OAuth 2.0 and OpenID Connect](https://www.youtube.com/watch?v=996OiexHze0)

#### UI/UX

- [AirBnB Building a Visual Language](https://airbnb.design/building-a-visual-language/)
- [My rules of solid design system](https://medium.com/@kamushken/my-rules-of-solid-design-system-4a7fe6c1811)
- [William Newton, Gusto - Building your design team's component library](https://medium.com/@willdjthrill/waiting-for-a-sign-to-start-building-your-design-teams-component-library-c43f4352c764)

#### API Design

- [Is GraphQL the end of RESTstyle APIs](https://www.youtube.com/watch?v=pi4HoCanLAk)
    - Fat clients & ever changing data (HATEOAS will require multiple RTT)
    - Over/underfetching doesn't represent what you need in the application
    - GraphQL is a declarative query language. Describe to the server what you have
    - Type safety, few RTT, 
- [Moving existing API from REST to GraphQL](https://www.youtube.com/watch?v=broQmxQAMjM)
    - takes multiple HTTP request to multiple endpoints to get what client wants. Optimized for servers instead of client
    - GraphQL is a specification. The query determines the response
    - GraphQL is typed
- [Smart Bear: How to design and document APIs with OpenAPI 3.0](https://www.youtube.com/watch?v=6kwmW_p_Tig)
- [How do search fit into a RESTful interfaces ?](https://softwareengineering.stackexchange.com/questions/233164/how-do-searches-fit-into-a-restful-interface)
    - using GET domain/Jobs?keyword={keywords}
    - using POST JobSearch create an actual search entity and returning a jobSearchId. Then GET jobs?jobSearch=jobSearchId returns the actual jobs collection
    - workaround with POST/PUT not recommended. GET is user and search engine friendly; idempotent. 
    - https://www.google.com/search?q=skeleton&tbas=0&tbm=isch&tbs=isz:l,ic:gray,itp:face,qdr:w,imgo:1
- [REST API Best Practices](https://saipraveenblog.wordpress.com/2014/09/29/rest-api-best-practices/)
    - safe , idempotent (multiple request return same response as single), cachable (not dependent on other process)
    - use plural nouns
    - do not use GET for state changes
    - use sub resources for relation GET /groups/{group id}/members
    - use HTTP header to specify serialization format
    - sorting by taking in comma separated variable. default value for sort parameter
    - field selection with parameter
    - handles pagination using link header
    - authentication: special http header, hmac, oauth
    - caching: last-modified, ETag using hash of content, 
    - response handling: JSON error response, [Common HTTP status code](https://saipraveenblog.wordpress.com/2014/09/29/common-http-status-codes/)
    - pretty print response
    - rate limiting: x-rate-limit-[limit, remaining, reset]
- [RESTful API Design Tips from Experience](https://medium.com/studioarmix/learn-restful-api-design-ideals-c5ec915a430f)
    - use access and refresh token
    - log in: check email and password hash against db. create new refresh and jwt token
    - having a /me as root 
    - from and limit parameter for pagination
    - health check endpoint: status and version
- [The Nuts and Bolts of API Security](https://www.youtube.com/watch?v=tj03NRM6SP8)
- [10 steps to build a strong Token based API Security](https://www.youtube.com/watch?v=r7ibZAPbg0k)

#### Web App Security

- [Learn Web Security with Google](https://www.youtube.com/watch?v=tgEIo7ZSkbQ)
- [OWASP Top 10 2017](https://www.youtube.com/watch?v=avFR_Af0KGk)
    - Injection
    - Broken authentication and session management: session hijacking, insufficient session timeout
    - Cross-Site Scripting: use content security policy and escape HTML output. Same-origin policy
    - Broken access control: guessing url gives you access to data
    - Security Misconfiguration: dont return detail error message to user
    - Sensitive Data Exposure: Use HSTS to force HTTPS. Make sure everything is encrypted
    - Cross-Site Request Forgery: Using your session token to initiate request
- [OWASP Top 10 2017 Paper](https://www.owasp.org/images/7/72/OWASP_Top_10-2017_%28en%29.pdf.pdf )
- [Web Application Best Practices](https://gearheart.io/blog/web-application-security-10-best-practices/)
    - use hashed password, transfer session id with cookies
    - encrypt information stored in cookies
- [User authentication best practices](https://techblog.bozho.net/user-authentication-best-practices-checklist/)
    - bcrypt (algorithm) with long salt is a bare minimum. other include scrypt, PBKDF2
    - [You are storing password wrongly](https://blog.codinghorror.com/youre-probably-storing-passwords-incorrectly/)
    - marks cookies as secure to prevent cookie theft
    - disallow framing
    - let user logout by deleting cookies and invalidating session

#### Frontend Design

##### [Android MVP vs MVVM](https://www.youtube.com/watch?v=ugpC98LcNqA)

- MVP
    - View and Presenter need reference to each other by implementing interfaces
    - 1-1 relationship between presenter and view
- MVVM
    - View notifies ViewModel about different actions
    - View holds reference of ViewModel but ViewModel unaware of View
    - View observes live data from ViewModel
- View is light on both cases just pure visualization. Business logic separate from UI

##### [MVC vs MVP vs MVVM vs MVI - Marcin Moskała
](https://www.youtube.com/watch?v=L634o_Rjly0)

- MVC allows for separation of concern
- MVC: Supervising Controller
    - View data bind to Model
    - Controller handles input changing the Model
- MVC: Passive View
    - Stateless view fully controlled by Controller
    - Separation between business logic and presentation logic
- MVP improve testability and independence
    - Hide view behind interface
- Clean Architecture: Entities < Use Cases < Presenters < UI
-  MVVM: View binded to ViewModel 
- MVI (Model View Intent): Everything is a stream. Producer > Process > Consumer


##### [Google PWA Checklist](https://developers.google.com/web/progressive-web-apps/checklist)

- [Implement Https](https://developers.google.com/web/fundamentals/security/encrypt-in-transit/enable-https) using [letsencrypt.org](https://letsencrypt.org/)
    - generate public/private rsa keypair
    - generate certificate signing request
    - submit CSR to certificate authority (CA)
- [Google Responsive Web Design Basic](https://developers.google.com/web/fundamentals/design-and-ux/responsive/) following [Udacity course](https://classroom.udacity.com/courses/ud893)
    - use meta viewport to control page scaling and dimension
    - use css media queries for different devices
    - use relative units
    - define breakpoint based on content
    - work on smallest device first
    - optimize text for rendering (70 - 80 characters per line)
- Use [Service Worker](https://developers.google.com/web/fundamentals/primers/service-workers/) to ensure all app URLs load while offline 
- Use [Web App Manifest](https://developers.google.com/web/fundamentals/web-app-manifest/) to allow app to be added to Home
- Improve [performance](https://developers.google.com/web/fundamentals/performance/why-performance-matters/) 
    - Use [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)
    - Use [PRPL](https://developers.google.com/web/fundamentals/performance/prpl-pattern/) pattern
- Page transition don't feel like they block on network
- Each page has a URL so deep-linkable

##### [React + Redux + Webpack](https://www.youtube.com/playlist?list=PLQDnxXqV213JJFtDaG0aE9vqvp6Wm7nBg)

##### [Progressive Web App tutorial – learn to build a PWA from scratch](https://www.youtube.com/watch?v=gcx-3qi7t7c)

##### [Building PWA with React](https://www.youtube.com/playlist?list=PLYHcCSWbw4G6s7NVi2RK0TKd69PdB0sWe)

##### [Reactive Programming with RxJS + React + Redux](https://www.youtube.com/watch?v=biVbj7b0M8I)

- Reactive programming is all about stream. Values on a timeline
- Observable creates stream that can be subscribed
- operating on stream create a new stream
- streamify anything that has event

##### [Javascript fundamentals before learning React](https://www.robinwieruch.de/javascript-fundamentals-react-requirements/)

##### [Essential React Libraries 2018](https://www.robinwieruch.de/essential-react-libraries-framework/)

##### [Learn React before using Redux](https://www.robinwieruch.de/learn-react-before-using-redux/)

##### [Tips to learn React Redux](https://www.robinwieruch.de/tips-to-learn-react-redux/)

##### [Getting started to learn Redux](https://egghead.io/lessons/react-redux-the-single-immutable-state-tree)

##### [Dan Abramov - The Redux Journey at react-europe 2016](https://www.youtube.com/watch?v=uvAXVMwHJXU)

- inspired by flux architecture. using a single store and follow a FP principle
- simply keep track of listeners and state. Dispatch action when needed 
- Redux constraints:
    - Single State Tree
    - Actions describe states
    - Reducer update states
- Redux Contracts
    - Reducer (state, action) -> state
    - Selector (state, args) -> derivation of state
    - Middleware store => next => action => any. Allow for custom dispatching
    - Enhancer (createStore => createStore) 

#### Redis

- [Top 5 uses of Redis as Database](https://www.youtube.com/watch?v=jTTlBc2-T9Q&t=385s)
    - in-memory key-data structure store
    - use as cache layer, 2nd Database
    - use as pub/sub engine
    - supports persistence and diferent data types besides key-value like memcached
    - use-cases:
        - show latest items
        - deletion and filtering
        - leaderboard and related problems
        - set expiration date
        - unique items 
        - real time analysis 
        - queue

#### Load Balancing

- Choosing between Nginx and HAProxy as load balancer
    - HAProxy provides more metrics 
    - Better integration with monitoring tools
- [Load Balancing with NGINX](https://www.youtube.com/watch?v=2X4ZO5tO7Co)
    - also terminates HTTPS, Serving static files
    - distribute load based on certain algorithm
    - failover: user is served by other node
- [Monitor and improve web performance with HAProxy](https://www.youtube.com/watch?v=maD5JMf_Hyw)

#### Monitoring & Alerting

- [InfluxDB CMU](https://www.youtube.com/watch?v=2SUBRE6wGiA)
    - regular time series and irregular time series
    - scalibility issue else can just use regular RDBMS
    - fast range queries, automatic downsampling
    - Time Structured Merge Tree 
        - Write Ahead Log (WAL) -> In memory cache -> Disk index (periodically flushes from in memory cache)
        - encode based on precision and deltas
    - Uses inverted index
- [Build a Search App with ElasticSearch](https://www.youtube.com/watch?v=9UEymp254kA)
- [Getting Down and Dirty with ELasticSearch](https://www.youtube.com/watch?v=7FLXjgB0PQI)
- [So why would I use distributed database like Apache Cassandra](https://www.youtube.com/watch?v=i3yUlwoCPzU)
- [Introduction to Apache Cassandra](https://www.youtube.com/watch?v=B_HTdrTgGNs)
- [Big Data Modeling with Cassandra](https://www.youtube.com/watch?v=L5xHQwT1Xww)

#### Distributed System

- [Distributed System in One Lesson](https://www.youtube.com/watch?v=Y6Ev8GIlbxc)
- [Martin Kleppmann Event Sourcing and Stream](https://www.youtube.com/watch?v=avi-TZI9t2I)
